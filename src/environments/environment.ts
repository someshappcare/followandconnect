// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBY2zADqiB-d3ZAx0q06VNIlqXsafmik44",
    authDomain: "followandconnect-66664.firebaseapp.com",
    databaseURL: "https://followandconnect-66664.firebaseio.com",
    projectId: "followandconnect-66664",
    storageBucket: "followandconnect-66664.appspot.com",
    messagingSenderId: "7871621202",
    appId: "1:7871621202:web:2f282ddd142dfc5728d78b",
    measurementId: "G-TXYB08386Q"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
