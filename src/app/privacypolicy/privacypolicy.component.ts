import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-privacypolicy',
  templateUrl: './privacypolicy.component.html',
  styleUrls: ['./privacypolicy.component.css']
})
export class PrivacypolicyComponent implements OnInit {

   privacylist = [];
  constructor(private restservice: RestapiService, private SpinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
    this.SpinnerService.show();
    this.restservice.privacypolicy().subscribe((list: any) => {
      // console.log(list);
      this.SpinnerService.hide();
      this.privacylist =list.data;
    },err => {
      this.SpinnerService.hide();
      console.log("error message"+err);
    });
  }
}
