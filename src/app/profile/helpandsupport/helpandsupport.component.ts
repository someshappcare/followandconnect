import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ConstImages } from 'src/app/helpers/const-images';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-helpandsupport',
  templateUrl: './helpandsupport.component.html',
  styleUrls: ['./helpandsupport.component.css']
})
export class HelpandsupportComponent implements OnInit {
  add = ConstImages.add;
  faqlist = [];

  constructor(private restservice: RestapiService, private SpinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
    this.SpinnerService.show();
    this.restservice.faqs().subscribe((list: any) => {
      console.log(list);
      this.SpinnerService.hide();
      this.faqlist= list.data;
    },err => {
      console.log("error message"+err);
    });
  }
}
