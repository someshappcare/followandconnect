import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ConstImages } from 'src/app/helpers/const-images';
import { Passwordmatcher } from 'src/app/helpers/passwordmatcher';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {

  changePasswordForm: FormGroup;
  submitted = false;
  submitt =false;
  changePasswordData: any;
  error = '';
  forgoterror ='';
  fieldTextTypePassword: any;
  confirm_text_password: any;
  new_confirm_text_password: any;
  // const images & validations
  oldpasswordValidation = ConstImages.oldpassword_validation;
  newpasswordValidation = ConstImages.newpassword_validation;
  confirmpasswordValidation = ConstImages.confirmpassword_validation;
  confirmpasswordvalidatorValidation = ConstImages.confirmpasswordvalidator_validation;
  usernameCross = ConstImages.username_cross;
  logo = ConstImages.logo;
  forgotForm: FormGroup;
  forgotpasswordemail;
  // const images & validations
  emailValidation = ConstImages.email_validation;
  emailpatternValidation = ConstImages.emailpattern_validation;


  toggleFieldPassword() {
    this.fieldTextTypePassword = !this.fieldTextTypePassword;
  }

  toggleConfirmFieldPassword() {
    this.confirm_text_password = !this.confirm_text_password;
  }

  newtoggleConfirmFieldPassword() {
    this.new_confirm_text_password = !this.new_confirm_text_password;
  }
  constructor(private toastr: ToastrService, private formBuilder: FormBuilder, private restservice: RestapiService, private SpinnerService: NgxSpinnerService) { }

  ngOnInit() {
    this.changePasswordForm = this.formBuilder.group({
      oldpassword: ['', [Validators.required]],
      newpassword: ['', [Validators.required]],
      confirm_password: ['', [Validators.required]]
    }, {
      validator: Passwordmatcher('newpassword', 'confirm_password')
    });

    this.forgotForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });

  }
  get f() { return this.changePasswordForm.controls; }
  userdetails = JSON.parse(localStorage.getItem("currentUser"));
  userid = this.userdetails[0].user_id;
  email = this.userdetails[0].email;

  onSubmit() {
    console.log(this.changePasswordForm.value);
    this.changePasswordData = {
      "currentpassword": this.changePasswordForm.value.oldpassword,
      "password": this.changePasswordForm.value.newpassword,
      "cpassword": this.changePasswordForm.value.confirm_password,
      "user_id": this.userid,
      "email": this.email,
    }
    this.submitted = true;
    console.log(this.changePasswordData);
    if (this.changePasswordForm.invalid) {
      return;
    }
    this.SpinnerService.show();
    console.log("before service");
    console.log(this.changePasswordData);
    this.restservice.changePassword(this.changePasswordData).subscribe((list: any) => {
      console.log(list.message);
      if (list.status == true) {
        console.log(list.message);
        this.toastr.warning('', list.message,{
          positionClass: 'toast-top-center'
     });
        this.SpinnerService.hide();
      } else {
        console.log(list.message);
        this.toastr.warning('', list.message,{
          positionClass: 'toast-top-center'
     });
        this.SpinnerService.hide();
      }
    }, err => {
      this.SpinnerService.hide();
      console.log(err);
    });
  }


  get fp() { return this.forgotForm.controls; }
  onforgotSubmit() {
    this.forgotpasswordemail = {
      "email": this.forgotForm.value.email,
    }
    this.submitt= true;
    console.log(this.forgotForm.value);
    if (this.forgotForm.invalid) {
      return;
    }
    this.SpinnerService.show();
    this.restservice.forgotPassword(this.forgotpasswordemail).subscribe((list: any) => {
      console.log(list);
      if(list.status ==false){
        this.toastr.warning('', list.message,{
          positionClass: 'toast-top-center'
     });
      }
      if(list.status ==true){
        this.toastr.warning('', list.message,{
          positionClass: 'toast-top-center'
     });
        this.SpinnerService.hide();
      }
    }, err => {
      this.SpinnerService.hide();
      console.log(err);
    });
  }

  usernameclear(){
    this.forgotForm.controls['email'].reset();
  }
  
}
