import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ConstImages } from 'src/app/helpers/const-images';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  search = ConstImages.search;
  profile = ConstImages.profile;
  settingsRightarrow = ConstImages.settings_rightarrow;
  defaultpic = ConstImages.defaultpic;
  blockusers: [];
  unblock = [];
  unblockusermodel: any;
  searchedKeywordforblockusers: string;
  constructor(private toastr: ToastrService,private router: Router, private restservice: RestapiService, private SpinnerService: NgxSpinnerService) { }

  ngOnInit() {

  }
  userdetails = JSON.parse(localStorage.getItem("currentUser"));
  userid = this.userdetails[0].user_id;

  account = {
    "uid": this.userid,
    "API-KEY": ConstImages.apikey
  }
  userBlock = {
    "uid": this.userid,
  }
  deleteaccount() {
    this.SpinnerService.show();
    this.restservice.delecteAccount(this.account).subscribe((list: any) => {
      console.log(list.message);
      localStorage.removeItem('currentUser');
      this.SpinnerService.hide();
      this.router.navigateByUrl("/login");
    });
  }

  blockedUsers() {
    console.log(this.userid);
    this.SpinnerService.show();
    this.restservice.blockUsersList(this.userBlock).subscribe((list: any) => {
      console.log(list.message);
      console.log(list.data);
      this.blockusers = list.data;
      this.SpinnerService.hide();
    });
  }

  unblockuserclick(unblock: any) {
    console.log("unblockuserclick");
    console.log(unblock);
    this.unblockusermodel = {
      "user_id": this.userid,
      "block_id": unblock
    }
    console.log(this.unblockusermodel);
    this.restservice.unblockUser(this.unblockusermodel).subscribe((list: any) => {
      console.log(list.message);
      this.toastr.warning('', list.message,{
        positionClass: 'toast-top-center'
   });
    });
  }

}
