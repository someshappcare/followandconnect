import { Component, OnInit } from '@angular/core';
import { ConstImages } from 'src/app/helpers/const-images';

@Component({
  selector: 'app-profilesidenav',
  templateUrl: './profilesidenav.component.html',
  styleUrls: ['./profilesidenav.component.css']
})
export class ProfilesidenavComponent implements OnInit {
  privacy = ConstImages.privacy;
  help = ConstImages.help;
  notifications = ConstImages.notifications;
  search = ConstImages.search;
  settings = ConstImages.settings;
  key = ConstImages.key;

  constructor() { }

  ngOnInit(): void {
  }

}
