import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilesidenavComponent } from './profilesidenav.component';

describe('ProfilesidenavComponent', () => {
  let component: ProfilesidenavComponent;
  let fixture: ComponentFixture<ProfilesidenavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilesidenavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilesidenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
