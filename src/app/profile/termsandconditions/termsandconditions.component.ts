import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-termsandconditions',
  templateUrl: './termsandconditions.component.html',
  styleUrls: ['./termsandconditions.component.css']
})
export class TermsandconditionsComponent implements OnInit {

  terms: [];
  constructor(private restservice: RestapiService, private SpinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
    this.SpinnerService.show();
    this.restservice.termsandconditions().subscribe((list: any) => {
      // console.log(list);
      this.SpinnerService.hide();
      this.terms =list.data;
   
    },err => {
      this.SpinnerService.hide();
      console.log("error message"+err);
    });
  }
}
