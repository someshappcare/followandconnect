import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {
  aboutUs = [];
  constructor(private restservice: RestapiService, private SpinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
    this.SpinnerService.show();
    this.restservice.aboutUs().subscribe((list: any) => {
      console.log(list);
      this.SpinnerService.hide();
      this.aboutUs =list.data;
   
    },err => {
      console.log("error message"+err);
    });
  }
}
