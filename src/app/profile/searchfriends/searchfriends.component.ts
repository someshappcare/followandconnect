import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ConstImages } from 'src/app/helpers/const-images';
import { RestapiService } from 'src/app/restapi.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-searchfriends',
  templateUrl: './searchfriends.component.html',
  styleUrls: ['./searchfriends.component.css']
})
export class SearchfriendsComponent implements OnInit {
  search = ConstImages.search;
  profile = ConstImages.profile;
  defaultpic = ConstImages.defaultpic1;
  searchmodel;
  friendstatusmodel: any;
  searchdatarecentmodel: any;
  searchlist = [];
  friendID: any;
  friendconnectionstatus: any;
  friend_status: any;
  searchname: any;
  searchrecentmodel: any;
  recentsearch: [];
  recentsearchpassdata: any;
  searchpassdata:any;
  userdetails: any;
  searchrecentmodellist: any;
  userid: any;
  constructor( private toastr: ToastrService, private router: Router, private restservice: RestapiService, private SpinnerService: NgxSpinnerService) { }

  ngOnInit() {
    this.userdetails = JSON.parse(localStorage.getItem("currentUser"));
    this.userid = this.userdetails[0].user_id;
    this.recentsearchlist();
  }
  recentsearchlist() {
    this.searchrecentmodel = {
      "uid": this.userid
    }
    this.restservice.searchlist(this.searchrecentmodel).subscribe((list: any) => {
      console.log(list);
      this.recentsearch = list.data;
    });
  }

  serachrecentfriendclick(recentuserid: any) {
    console.log("serachrecentfriendclick");
    console.log(recentuserid);
    this.searchrecentmodellist = {
      "uid": this.userid,
      "fid": recentuserid
    }
    this.recentsearchpassdata = [ {
    user_id:recentuserid
    }];
    console.log(this.searchrecentmodellist);
    this.restservice.friendStatus(this.searchrecentmodellist).subscribe((list: any) => {
      console.log("getUserFriendStatus");
      console.log(list);
      console.log(list.user_info);
      this.friendconnectionstatus = list.connection_status;
      this.friend_status = list.friend_status;
      console.log(this.friendconnectionstatus, this.friend_status);
      if (this.friend_status == 'noFriend') {
        if (this.friendconnectionstatus == 'noFollowing') {
          this.router.navigate(['followProfile'], { queryParams: this.recentsearchpassdata[0] });
          // this.router.navigate( ['followProfile'], { queryParams: this.searchlist[0], skipLocationChange: true});
        }
        if (this.friendconnectionstatus == 'inFollowing') {
          this.router.navigate(['followingProfile'], { queryParams: this.recentsearchpassdata[0]});
        }
      }
      if (this.friend_status == 'inFriend') {
        this.router.navigate(['friendsProfile'], { queryParams: this.recentsearchpassdata[0] });
      }
    });
    }

  searchkey(searchvalue) {
    this.searchname = searchvalue;
    console.log(this.searchname);
    this.searchmodel = {
      "username": this.searchname,
      "uid": this.userid,
    }
    console.log(this.searchmodel);
    if(this.searchname!=""){
    this.searchusers();
    }else{
      console.log("No search data");
    }
  }
  searchusers() {
    this.SpinnerService.show();
    this.restservice.searchfriends(this.searchmodel).subscribe((list: any) => {
      console.log(list.message);
      console.log(list);
      console.log(list.data);
      if (list.data.length > 0) {
        this.searchlist = list.data;
        this.friendID = list.data[0].user_id;
        console.log(this.friendID);
        this.searchpassdata = [ {
          user_id:this.friendID
          }];
        this.getUserFriendStatus();
        this.SpinnerService.hide();
      } else{
          this.toastr.warning('', "User Not Found!", {
            positionClass: 'toast-top-center'
          });
      }
    });
  }

  serachfriendclick() {
    this.getsearchdatainsert();
    console.log("click");
    if (this.friend_status == 'noFriend') {
      if (this.friendconnectionstatus == 'noFollowing') {
        this.router.navigate(['followProfile'], { queryParams: this.searchpassdata[0]});
        // this.router.navigate( ['followProfile'], { queryParams: this.searchlist[0], skipLocationChange: true});
      }
      if (this.friendconnectionstatus == 'inFollowing') {
        this.router.navigate(['followingProfile'], { queryParams: this.searchpassdata[0]});
      }
    }
    if (this.friend_status == 'inFriend') {
      this.router.navigate(['friendsProfile'], { queryParams: this.searchpassdata[0]});
    }
  }

  getUserFriendStatus() {
    console.log("getUserFriendStatus");
    this.friendstatusmodel = {
      "uid": this.userid,
      "fid": this.friendID
    }
    console.log(this.friendstatusmodel);
    this.restservice.friendStatus(this.friendstatusmodel).subscribe((list: any) => {
      console.log("getUserFriendStatus");
      console.log(list);
      this.friendconnectionstatus = list.connection_status;
      this.friend_status = list.friend_status;
      console.log(this.friendconnectionstatus, this.friend_status);
    });
  }

  getsearchdatainsert() {
    console.log("getsearchdatainsert");
    this.searchdatarecentmodel = {
      "uid": this.userid,
      "query": this.searchname,
      "fid": this.friendID
    }
    console.log(this.searchdatarecentmodel);
    this.restservice.searchdatainsert(this.searchdatarecentmodel).subscribe((list: any) => {
      console.log("getUserFriendStatus");
      console.log(list.message);
    });
  }


}