import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

import { ConstImages } from 'src/app/helpers/const-images';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  notifictionlist = [];
  notificationImage = ConstImages.notification_image;
  defaultpic = ConstImages.defaultpic;
constructor(private router:Router, private restservice: RestapiService, private SpinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
    this.notificationlist();
  }
  userdetails = JSON.parse(localStorage.getItem("currentUser"));
    userid= this.userdetails[0].user_id;

    notification={
    "uid": this.userid,
    "API-KEY": ConstImages.apikey
  }

   notificationlist(){
    this.SpinnerService.show();
    this.restservice.notifications(this.notification).subscribe((list: any) => {
    console.log(list);
    this.notifictionlist = list.data;
    this.SpinnerService.hide();
   });
  }

}
