import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConstImages } from 'src/app/helpers/const-images';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  logo = ConstImages.logo;
  profile = ConstImages.profile;
  sideDots = ConstImages.side_dots;
  logout = ConstImages.logout;
  notifications = ConstImages.notifications;
  search = ConstImages.search;
  settings = ConstImages.settings;
  userdetails: any;
  profilepic:any;
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.userdetails = JSON.parse(localStorage.getItem("currentUser"));
    this.profilepic = this.userdetails[0].profile_pic;
      console.log("navbar");
    console.log(this.profilepic);
  }
  logoutclick() {
    console.log("logout.............")
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
    console.log(JSON.parse(localStorage.getItem("token")));
    this.router.navigateByUrl("/login");
  }
}