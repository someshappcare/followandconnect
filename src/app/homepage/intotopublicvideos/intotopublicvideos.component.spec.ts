import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntotopublicvideosComponent } from './intotopublicvideos.component';

describe('IntotopublicvideosComponent', () => {
  let component: IntotopublicvideosComponent;
  let fixture: ComponentFixture<IntotopublicvideosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntotopublicvideosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntotopublicvideosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
