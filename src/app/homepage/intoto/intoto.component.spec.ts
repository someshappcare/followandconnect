import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntotoComponent } from './intoto.component';

describe('IntotoComponent', () => {
  let component: IntotoComponent;
  let fixture: ComponentFixture<IntotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
