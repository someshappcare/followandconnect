import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntotoprivateComponent } from './intotoprivate.component';

describe('IntotoprivateComponent', () => {
  let component: IntotoprivateComponent;
  let fixture: ComponentFixture<IntotoprivateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntotoprivateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntotoprivateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
