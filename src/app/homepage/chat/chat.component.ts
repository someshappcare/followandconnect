import { Component,ElementRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ConstImages } from 'src/app/helpers/const-images';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  search = ConstImages.search;
  defaultpic = ConstImages.defaultpic;
  add = ConstImages.add;
  firendslistmodel: any;
  friendlistArray: [];
  chatHistoryArray: [];
  chathistorymodel: any;
  senddatamodel: any;
  toid: any;
  name = '';
  pic = ''; clearinput: any;
  senddata: any; interval: any; timer: any;
  toiidsend: any; namesend: any; picsend: any;
  constructor(private _el: ElementRef, private router: Router, private restservice: RestapiService, private SpinnerService: NgxSpinnerService) { }
  searchedKeywordforchat: string;
  visibility = false;
  userdetails = JSON.parse(localStorage.getItem("currentUser"));
  userid = this.userdetails[0].user_id;
  ngOnInit(): void {
    this.getfriendslist();
   // this.timer= this.interval();
  }

  public scrollToBottom() {
    const el: HTMLDivElement = this._el.nativeElement;
    el.scrollTop = Math.max(0, el.scrollHeight - el.offsetHeight);
  }

  getfriendslist() {
    this.firendslistmodel = {
      "uid": this.userid,
    }
    console.log(this.firendslistmodel);
    this.SpinnerService.show();
    this.restservice.chatfriendlist(this.firendslistmodel).subscribe((list: any) => {
      console.log(list);
      this.SpinnerService.hide();
      this.friendlistArray = list.data;
    });
  }

  chathistorybyId(list: any, list1: any, list2: any) {
    this.SpinnerService.show();
    this.visibility = true;
    this.toid = list;
    this.name = list1;
    this.pic = list2;
    console.log(this.toid, list1, list2);
    localStorage.setItem('toidrepeat', JSON.stringify(this.toid));
    localStorage.setItem('namerepet', JSON.stringify(this.name));
    localStorage.setItem('picrepeat', JSON.stringify(this.pic));
    this.interval =setInterval(() => {
      this.chathistorybyIdloop();
     }, 3000);
     this.scrollToBottom();
  }
    chathistorybyIdloop(){
    this.chathistorymodel = {
      "from_id": this.userid,
      "to_id": this.toid,
    }
    this.restservice.chatHistory(this.chathistorymodel).subscribe((list: any) => {
      console.log(list);
      this.SpinnerService.hide();
      if(list.data.length> 0){
      this.chatHistoryArray = list.data;
      }
      else{
        console.log(list.data.fullname);
      }
    });
  }

  sendmessage(senddata: any) {
    this.senddata = senddata;
    console.log(senddata);
    this.senddatamodel = {
      "message": senddata,
      "from_id": this.userid,
      "to_id": this.toid,
      "uid": this.userid
    }
    console.log(this.senddatamodel);
    this.chatsendmsg();
    this.clearinput ='';
  }

  chatsendmsg() {
    this.restservice.chatSend(this.senddatamodel).subscribe((list: any) => {
      console.log(list.message);
      console.log(list);
      this.toiidsend = JSON.parse(localStorage.getItem("toidrepeat"));
      this.namesend = JSON.parse(localStorage.getItem("namerepet"));
      this.picsend = JSON.parse(localStorage.getItem("picrepeat"));
      this.chathistorybyId(this.toiidsend, this.namesend, this.picsend);
    });
  }

  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    //  clearInterval(this.timer);
    }
  }

}
