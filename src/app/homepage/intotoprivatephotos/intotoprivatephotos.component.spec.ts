import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntotoprivatephotosComponent } from './intotoprivatephotos.component';

describe('IntotoprivatephotosComponent', () => {
  let component: IntotoprivatephotosComponent;
  let fixture: ComponentFixture<IntotoprivatephotosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntotoprivatephotosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntotoprivatephotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
