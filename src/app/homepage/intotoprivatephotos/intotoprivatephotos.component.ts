import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { RestapiService } from 'src/app/restapi.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-intotoprivatephotos',
  templateUrl: './intotoprivatephotos.component.html',
  styleUrls: ['./intotoprivatephotos.component.css']
})
export class IntotoprivatephotosComponent implements OnInit {

  viewintotolist: [];
  userdetails: any;
  userid: any;
  intoto: any;
  element: any; error: any;
  constructor(private router: Router, private restservice: RestapiService, private SpinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
    this.userdetails = JSON.parse(localStorage.getItem("currentUser"));
    this.userid = this.userdetails[0].user_id;
    this.getintotolist();
  }

  getintotolist() {
    this.intoto = {
      "uid": this.userid
    }
    this.SpinnerService.show();
    this.restservice.getIntoto(this.intoto).subscribe((list: any) => {
      console.log(list);
      console.log(list.data.imgf);
      this.viewintotolist = list.data;
      this.SpinnerService.hide();
      // console.log(this.viewintotolist.imgf);
      });
      // console.log(list.data.feed_list[].date);
      // console.log(list.data.feed_list.date);
      // console.log(list.data.Public.images);
  }
  fullviewclick(p: any){
    console.log(p);
    const type ="image";
    this.router.navigate(['fullview'], { queryParams: { id: p ,type:type, skipLocationChange: true} });
  }
}

