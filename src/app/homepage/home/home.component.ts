import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';
import { ConstImages } from 'src/app/helpers/const-images';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],

})
export class HomeComponent implements OnInit {

  left_image = ConstImages.left_image;
  like = ConstImages.like;
  likeColor = ConstImages.likeColor;
  dislike = ConstImages.dislike;
  dislikeColor = ConstImages.dislikeColor;
  add = ConstImages.add;
  eye = ConstImages.eye;
  comment = ConstImages.comment;
  sideDots = ConstImages.side_dots;
  upload_cross = ConstImages.upload_cross;
  profile = ConstImages.profile;
  defaultpic = ConstImages.defaultpic;
  commentForm: FormGroup;
  submitted = false;
  uploadform: FormGroup;
  editfeedform: FormGroup;
  url: any;
  format: any;
  file: any;
  imageType: any;
  feedpostdata: any;
  feedgetdata: any;
  userdetails: any;
  feedcommentdata: any;
  feedcommentmessage: any;
  feedlikedata: any;
  userid: any;
  uploadusername: any;
  uploadfullname: any;
  sidfeedcomment: any;
  sidfeedlike: any;
  viewlist: [];
  imagestofile = []; videostofile = []; editimages = []; editimagestext = []; editvideos = [];
  editvideostext = []; indexselectedvideos: any;
  commentlist: [];
  unlikestatus: any;
  sid: any; puid: any; cid: any;
  selectedfile: any;
  arrayBuffer: any;
  uploadprofile_pic: any;
  fileimage; filevideo;
  picvideoselected: []; indexselected: any;
  blockuserModel: any; editfeeddescription: any;
  mydata = []; image: any; video: any;
  myimageFiles: string[] = [];
  myvideoFiles: string[] = [];
  fullviewclickdata: any;
  fullviewclickdatatype: any;
  @ViewChild('closeuploadmodel') closeuploadmodel;
  @ViewChild('closecommentmodel') closecommentmodel;
  @ViewChild('closeeditmodel') closeeditmodel;
  @ViewChild('exampleModal') exampleModal;
  @ViewChild('closedropdown') closedropdown;
   @ViewChild('imagepopupModel') imagepopupModel: any;
arrowright = ConstImages.arrowright;
arrowleft = ConstImages.arrowleft;
  public element; token: any;
  public error;
  viewfeedmodel: any;
  deletefeedmodel: any;
  editfeedmodel: any;
  editfeeddetailslist: any;
  show: boolean = false;
  constructor(private http: HttpClient, private router: Router, private toastr: ToastrService, private formBuilder: FormBuilder, private restservice: RestapiService, private SpinnerService: NgxSpinnerService) { }
  likestatus = '';
  category: any[] = [
    { value: '0', option: 'Private' },
    { value: '1', option: 'Public' }
  ];
  selectedcategory = 'Public';
  showModal() {
    this.show = !this.show;
  }
  ngOnInit() {
    this.uploadform = this.formBuilder.group({
      category: ['', []],
      description: ['', []]
    });

    this.editfeedform = this.formBuilder.group({
      editcategory: ['', []],
      editdescription: ['', []]
    });

    this.commentForm = this.formBuilder.group({
      comment: ['', []]
    });
    this.SpinnerService.show();
    console.log(localStorage.getItem('currentUser'));
    this.userdetails = JSON.parse(localStorage.getItem("currentUser"));
    this.userid = this.userdetails[0].user_id;
    this.uploadusername = this.userdetails[0].username;
    this.uploadfullname = this.userdetails[0].fullname;
    this.uploadprofile_pic = this.userdetails[0].profile_pic;
    this.token = this.userdetails[0].devicetoken;
    console.log(this.userid);
    if(this.userid ==''){
      this.router.navigate(['login']);
    }
    this.getfeed();
  }
  get f() { return this.editfeedform.controls; }

  getfeed() {
    this.feedgetdata = {
      "uid": this.userid
    }
    this.SpinnerService.show();
    this.restservice.feeddisplay(this.feedgetdata).subscribe((list: any) => {
      console.log(list);
      this.viewlist = list.data;
      if (list.data.length > 0) {
        this.puid = list.data[0].feed_list.puid;
        this.cid = list.data[0].feed_list.puid;
      }
      this.SpinnerService.hide();
    });
  }
  feedcommentlist(list: any) {
    this.sidfeedcomment = list;
    console.log("feedcommentlist");
    console.log(this.sidfeedcomment);
    this.feedcommentdata = {
      "feed_id": this.sidfeedcomment,
    }
    this.SpinnerService.show();
    this.restservice.feedcommentList(this.feedcommentdata).subscribe((list: any) => {
      console.log(list);
      this.commentlist = list.data;
      console.log(this.commentlist);
      this.SpinnerService.hide();
    });
  }
  feedlikeslist(list: any) {
    console.log(list);
    this.feedlikedata = {
      "feed_id": list,
      "poster_id": this.puid,
      "commenter_id": this.cid,
      "like": "1",
      "share": "",
      "comment": "",
      "dislike": "",
      "view": "",
      "devicetype": "web"
    }
    console.log(this.feedlikedata);
    // this.SpinnerService.show();
    this.restservice.feedcomment(this.feedlikedata).subscribe((list: any) => {
      console.log(list);
      console.log(list.message);
      console.log(list.status);
      this.likestatus = list.message;
      this.toastr.warning('', list.message, {
        positionClass: 'toast-top-center'
      });
      this.getfeed();
      // this.SpinnerService.hide();
    });
  }
  feeddislikeslikeslist(list: any) {
    console.log(list);
    this.feedlikedata = {
      "feed_id": list,
      "poster_id": this.puid,
      "commenter_id": this.cid,
      "like": "0",
      "share": "",
      "comment": "",
      "dislike": "",
      "view": "",
      "devicetype": "web"
    }

    console.log(this.feedlikedata);
    // this.SpinnerService.show();
    this.restservice.feedcomment(this.feedlikedata).subscribe((list: any) => {
      console.log(list);
      console.log(list.message);
      console.log(list.status);
      this.likestatus = list.message;
      this.toastr.warning('', list.message, {
        positionClass: 'toast-top-center'
      });
      this.getfeed();
      // this.SpinnerService.hide();
    });
  }
  feeddisdislikeslikeslistblack(list: any) {
    console.log(list);
    console.log("feeddisdislikeslikeslistblack");
    this.feedlikedata = {
      "feed_id": list,
      "poster_id": this.puid,
      "commenter_id": this.cid,
      "like": "",
      "share": "",
      "comment": "",
      "dislike": "1",
      "view": "",
      "devicetype": "web"
    }
    console.log(this.feedlikedata);
    // this.SpinnerService.show();
    this.restservice.feedcomment(this.feedlikedata).subscribe((list: any) => {
      console.log(list);
      console.log(list.message);
      console.log(list.status);
      this.likestatus = list.message;
      this.toastr.warning('', list.message, {
        positionClass: 'toast-top-center'
      });
      this.getfeed();
      // this.SpinnerService.hide();
    });
  }
  feeddisdislikeslikeslistcolor(list: any) {
    console.log(list);
    console.log("feeddisdislikeslikeslistcolor");
    this.feedlikedata = {
      "feed_id": list,
      "poster_id": this.puid,
      "commenter_id": this.cid,
      "like": "",
      "share": "",
      "comment": "",
      "dislike": "0",
      "view": "",
      "devicetype": "web"
    }
    console.log(this.feedlikedata);
    // this.SpinnerService.show();
    this.restservice.feedcomment(this.feedlikedata).subscribe((list: any) => {
      console.log(list);
      console.log(list.message);
      console.log(list.status);
      this.likestatus = list.message;
      this.toastr.warning('', list.message, {
        positionClass: 'toast-top-center'
      });
      this.getfeed();
      // this.SpinnerService.hide();
    });
  }

  commentsubmit() {
    this.submitted = true;
    this.feedcommentmessage = {
      "feed_id": this.sidfeedcomment,
      "poster_id": this.puid,
      "commenter_id": this.cid,
      "like": "",
      "share": "",
      "comment": this.commentForm.value.comment,
      "dislike": "",
      "view": "",
      "devicetype": "web"
    }
    console.log(this.feedcommentmessage);
    this.restservice.feedcomment(this.feedcommentmessage).subscribe((list: any) => {
      console.log(list.message);
      this.toastr.warning('', list.message, {
        positionClass: 'toast-top-center'
      });
      this.commentForm.controls['comment'].reset();
      this.closecommentmodel.nativeElement.click();
      this.getfeed();
    });
  }

  uploadsubmit() {
    console.log("upload");
    console.log(this.uploadform.value);
    const formData = new FormData();
    formData.append('uid', this.userid);
    formData.append('API-KEY', ConstImages.apikey);
    formData.append('feedtext', this.uploadform.value.description);
    if(this.uploadform.value.category==''){
    formData.append('privicy', this.selectedcategory);
    }else{
      formData.append('privicy', this.uploadform.value.category);
    }
    formData.append('address', 'hyderabad');
    formData.append('ltd', '17.26');
    formData.append('lng', '78.25');
    formData.append('isspoolvid', 'No');
      for (var i = 0; i < this.myimageFiles.length; i++) {
        formData.append("images[]", this.myimageFiles[i]);
        formData.append('videos[]', "");
      }
      console.log("videos");
      for (var i = 0; i < this.myvideoFiles.length; i++) {
        formData.append("images[]", "");
        formData.append('videos[]', this.myvideoFiles[i]);
      }
    if (this.myvideoFiles.length > 0 || this.myimageFiles.length > 0 || this.uploadform.value.description != '') {
      console.log("details", this.myvideoFiles.length,this.myimageFiles.length,this.uploadform.value.description );
      this.SpinnerService.show();
      this.restservice.feedpost(formData).subscribe((list: any) => {
        console.log(list.message);
        this.toastr.warning('', list.message, {
          positionClass: 'toast-top-center'
        });
        this.mydata = [];
        this.myimageFiles = [];
        this.myvideoFiles = [];
        this.getfeed();
        this.uploadform.controls['description'].reset();
        this.closeuploadmodel.nativeElement.click();
        this.SpinnerService.hide();
      });
    }
  }

  onSelectFile(event: any) {
    const files = event.target.files;
    if (files) {
      console.log("files", files);
      for (const file of files) {
        const reader = new FileReader();
        reader.onload = (e: any) => {
          if (file.type.indexOf("image") > -1) {
            this.mydata.push({
              url: e.target.result,
              type: 'img'
            });
            console.log("image");
            console.log("mydata", this.mydata);
            this.myimageFiles.push(file);
            console.log("loop", file, this.myimageFiles);

          } else if (file.type.indexOf("video") > -1) {
            this.mydata.push({
              url: e.target.result,
              type: 'video'
            });
            console.log("video");
            this.myvideoFiles.push(file);
            console.log("loop", this.myvideoFiles);
          }
        };
        reader.readAsDataURL(file);
      }
    }
  }


  blockuser(user_id: any) {
    this.blockuserModel = {
      "user_id": this.userid,
      "block_id": user_id
    }
    console.log(this.blockuserModel);
    this.restservice.blockUser(this.blockuserModel).subscribe((list: any) => {
      if (list.status == true) {
        console.log(list.message);
        this.toastr.warning('', list.message, {
          positionClass: 'toast-top-center'
        });
        this.getfeed();
      } else {
        console.log(list.message);
        alert(list.message);
      }
    }, err => {
      console.log("error message");
    });
  }


  deletefeeddetails(id: any, feedid: any) {
    console.log(id, feedid);
    this.deletefeedmodel = {
      "uid": id,
      "feed_id": feedid
    }
  }

  deletefeed() {
    console.log(this.deletefeedmodel);
    this.restservice.deleteuserfeed(this.deletefeedmodel).subscribe((list: any) => {
      console.log(list.message);
      this.toastr.warning('', list.message, {
        positionClass: 'toast-top-center'
      });
      console.log(list);
      this.getfeed();
    });
  }

  editUserdetails(view: any) {
    console.log(view.user_id);
    console.log("editfeed", view)
    this.editfeeddetailslist = view;
    this.editfeeddescription = this.editfeeddetailslist.feed_list.feed;
    this.editimages = this.editfeeddetailslist.imgf_file?.split(',');
    this.editvideos = this.editfeeddetailslist.vf_file?.split(',');
    this.editimagestext = this.editfeeddetailslist.feed_list.imgf?.split(',');
    this.editvideostext = this.editfeeddetailslist.feed_list.vf?.split(',');
    this.indexselected = this.editimages.length;
    this.indexselectedvideos = this.editvideos.length;
    console.log(this.editfeeddetailslist.feed_list.sid, this.editfeeddescription, this.editimages, this.editvideos, this.editimagestext, this.editvideostext);
    if (this.editfeeddetailslist.imgf_file != '') {
      for (var i = 0; i < this.editimages.length; i++) {
        this.getImage(this.editimages[i], this.editimagestext[i]).subscribe((response: any) => {
          this.imagestofile.push(response);
          this.myimageFiles.push(response);
          console.log(this.imagestofile, response);
        }
        );
      }
    }

    if (this.editfeeddetailslist.vf_file != '') {
      for (var i = 0; i < this.editvideos.length; i++) {
        this.getImage(this.editvideos[i], this.editvideostext[i]).subscribe((response: any) => {
          this.videostofile.push(response);
          this.myvideoFiles.push(response);
          console.log(this.videostofile, response);
        }
        );
      }
    }

  }
  getImage(i: any, text: any) {
    return this.http
      .get(i, {
        responseType: "arraybuffer"
      })
      .pipe(
        map(response => {
          return new File([response], text);
        })
      );
  }

  editfeedsubmit() {
    const editformData = new FormData();
    editformData.append('uid', this.userid);
    editformData.append('API-KEY', ConstImages.apikey);
    editformData.append('feedtext', this.editfeedform.value.editdescription);
    if(this.editfeedform.value.editcategory==''){
      editformData.append('privicy', this.selectedcategory);
    }else{
    editformData.append('privicy', this.editfeedform.value.editcategory);
    }
    editformData.append('address', 'hyderabad');
    editformData.append('ltd', '17.26');
    editformData.append('lng', '78.25');
    editformData.append('isspoolvid', 'No');
    editformData.append('feed_id', this.editfeeddetailslist.feed_list.sid);
    for (var i = 0; i < this.myimageFiles.length; i++) {
      editformData.append("images[]", this.myimageFiles[i]);
      editformData.append('videos[]', "");
    }
    for (var i = 0; i < this.myvideoFiles.length; i++) {
      editformData.append("images[]", "");
      editformData.append('videos[]', this.myvideoFiles[i]);
    }
    console.log(this.editfeedform.value);
    console.log(editformData);
    if (this.myvideoFiles.length > 0 || this.myimageFiles.length > 0 || this.editfeedform.value.editdescription != '') {
      this.SpinnerService.show();
      this.restservice.edituserfeed(editformData).subscribe((list: any) => {
        console.log(list.message);
        this.toastr.warning('', list.message, {
          positionClass: 'toast-top-center'
        });
        console.log("after edit upload", this.myimageFiles, this.myvideoFiles, this.mydata);
        this.mydata = [];
        this.myimageFiles = [];
        this.myvideoFiles = [];
        this.getfeed();
        console.log("after getfeed edit upload", this.myimageFiles, this.myvideoFiles, this.mydata);
        this.closeeditmodel.nativeElement.click();
        this.SpinnerService.hide();
      });
    }
  }

  sharefeeddetails(view) {
    if (view.imgf_file != '') {
      this.image = view.imgf_file.split(',').join("\n");
      console.log(this.image);
    } else {
      this.image = view.vf_file.split(',').join("\n");
      console.log(this.image);
    }
  }

  fullviewclick(p: any, feedid: any) {
    console.log("fullviewclick", p, "feedid", feedid);
    const type = "image";
    this.viewfeedmodel = {
      "uid": this.userid,
      "feed_id": feedid
    }
    console.log(this.viewfeedmodel);
    this.restservice.viewcount(this.viewfeedmodel).subscribe((list: any) => {
      console.log(list.message);
      console.log(list);
    });
    this.fullviewclickdata =p;
    this.fullviewclickdatatype =type;
      console.log("fullviewclickdata",this.fullviewclickdata);
    // this.router.navigate(['fullview'], { queryParams: { id: p, type: type, skipLocationChange: true } });
        this.imagepopupModel.nativeElement.click();
  }

  fullviewclick1(p: any, feedid: any) {
    console.log("fullviewclick", p, "feedid", feedid);
    const type = "video";
    this.viewfeedmodel = {
      "uid": this.userid,
      "feed_id": feedid
    }
    console.log(this.viewfeedmodel);
    this.restservice.viewcount(this.viewfeedmodel).subscribe((list: any) => {
      console.log(list.message);
      console.log(list);
    });
      this.fullviewclickdata =p;
    this.fullviewclickdatatype =type;
      console.log("fullviewclickdata",this.fullviewclickdata);
    // this.router.navigate(['fullview'], { queryParams: { id: p, type: type, skipLocationChange: true } });
        this.imagepopupModel.nativeElement.click();
    // this.router.navigate(['fullview'], { queryParams: { id: p, type: type, skipLocationChange: true } });
  }

  removeimageSelectedFile(index: any) {
    this.mydata.splice(index, 1);
    this.myimageFiles.splice(index, 1);
    console.log("removeSelectedFile", this.mydata, this.myimageFiles);
  }
  removevideosSelectedFile(index: any) {
    this.mydata.splice(index, 1);
    this.myvideoFiles.splice(index, 1);
    console.log("removeSelectedFile", this.mydata, this.myvideoFiles, index);
  }

  removeeditimageSelectedFile(index: any) {
    console.log("removeSelectedFileindex",index);
    this.editimages.splice(index, 1);
    this.myimageFiles.splice(index, 1);
    this.indexselected = this.editimages.length;
    console.log("removeeditimageSelectedFile", this.editimages, this.myimageFiles, this.indexselected, index);
  }
  removeeditimageuploadSelectedFile(index: any) {
    this.mydata.splice(index, 1);
    this.myimageFiles.splice(this.indexselected + index, 1);
    console.log("removeeditimageuploadSelectedFile", this.mydata, this.myimageFiles, this.indexselected + index, this.editimages.length, index);
  }

  removeeditvideoSelectedFile(index: any) {
    this.editvideos.splice(index, 1);
    this.myvideoFiles.splice(index, 1);
    this.indexselectedvideos = this.editvideos.length;
    console.log("removeeditvideoSelectedFile", this.editvideos);
  }
  removeeditvideosuploadSelectedFile(index: any) {
    this.mydata.splice(index, 1);
    this.myvideoFiles.splice(this.indexselected + index, 1);
    console.log("removeeditvideoSelectedFile", this.mydata, this.myvideoFiles);
  }
}