import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ConstImages } from 'src/app/helpers/const-images';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-followprofile',
  templateUrl: './followprofile.component.html',
  styleUrls: ['./followprofile.component.css']
})
export class FollowprofileComponent implements OnInit {

  like = ConstImages.like;
  dislike = ConstImages.dislike;
  likeColor = ConstImages.likeColor;
  dislikeColor =ConstImages.dislikeColor;
  eye = ConstImages.eye;
  comment = ConstImages.comment;
  sideDots = ConstImages.side_dots;
  followerprofile = ConstImages.followerprofile;
  userlike = ConstImages.userlike;
  search = ConstImages.search;
  profile = ConstImages.profile;
  defaultpic = ConstImages.defaultpic;
  username: any;
  useremail: any;
  followerscount: any;
  followdata: [];
  user_id: any;
  feedgetdata: any;
  feedpostdata: any;
  feedcommentdata: any;
  followusermodel: any;
  followerlistmodel: any;
  feedcommentmessage: any;
  myviewForm: FormGroup;
  commentForm: FormGroup;
  submitted = false;
  fullname:any;
  viewlist: [];
  commentlist: [];
  viewfollowerslist: [];
  friendstatus:[];
  
  userData: any;
  sid: any; puid: any; cid: any;
  @ViewChild('closecommentmodel') closecommentmodel;
  searchedKeywordforfollowers: string;
  friendstatusmodel;
  profilepic:any;
  searchpassdata: any;
  sidfeedcomment: any;
  feedlikedata: any;
  likestatus: any;
  element: any; error: any;
   @ViewChild('imagepopupModel') imagepopupModel: any;
arrowright = ConstImages.arrowright;
arrowleft = ConstImages.arrowleft;
fullviewclickdata: any;
  fullviewclickdatatype: any; Publiccheck: any;
  constructor(private toastr: ToastrService, private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder, private restservice: RestapiService, private SpinnerService: NgxSpinnerService) { }
  userdetails = JSON.parse(localStorage.getItem("currentUser"));
  userid = this.userdetails[0].user_id;
  token =this.userdetails[0].devicetoken;
  ngOnInit() {
    this.Publiccheck ="Public";
    this.token = JSON.parse(localStorage.getItem("token"));
    this.route.queryParams.subscribe(
      params => {
        console.log(params);
        this.userData = params;
        this.user_id =params.user_id;
        this.getuserfeed();
      });
      this.commentForm = this.formBuilder.group({
        comment: ['', []]
      });
  }
  getuserfeed(){
    this.feedgetdata = {
      "uid": this.user_id
    }
    this.SpinnerService.show();
    this.restservice.getuserprofilefeed(this.feedgetdata).subscribe((list: any) => {
      console.log(list);
      this.fullname = list.user_info[0].fullname;
      console.log(list.user_info[0].fullname);
      this.username = list.user_info[0].username;
      this.profilepic = list.user_info[0].profile_pic;
      this.useremail = list.user_info[0].email;
      if(list.data.length>0){
        this.puid = list.data[0].feed_list.puid;
        this.cid =list.data[0].feed_list.puid;
       }
      this.followerscount = list.followers;
      this.viewlist = list.data;
      this.SpinnerService.hide();
    });
  }

  feedcommentlist(list: any) {
    this.sidfeedcomment = list;
    console.log("feedcommentlist");
    console.log(this.sidfeedcomment);
    this.feedcommentdata = {
      "feed_id": this.sidfeedcomment,
    }
    this.SpinnerService.show();
    this.restservice.feedcommentList(this.feedcommentdata).subscribe((list: any) => {
      console.log(list);
      this.commentlist = list.data;
      console.log(this.commentlist);
      this.SpinnerService.hide();
    });
  }
  feedlikeslist(list: any) {
    console.log(list);
    this.feedlikedata = {
      "feed_id": list,
      "poster_id": this.puid,
      "commenter_id": this.cid,
      "like": "1",
      "share": "",
      "comment": "",
      "dislike": "",
      "view": "",
      "devicetype": "web"
    }
    console.log(this.feedlikedata);
    // this.SpinnerService.show();
    this.restservice.feedcomment(this.feedlikedata).subscribe((list: any) => {
      console.log(list);
      console.log(list.message);
      console.log(list.status);
      this.likestatus = list.message;
      this.toastr.warning('', list.message,{
        positionClass: 'toast-top-center'
   });
      this.getuserfeed();
      // this.SpinnerService.hide();
    });
  }
  feeddislikeslikeslist(list: any) {
    console.log(list);
    this.feedlikedata = {
      "feed_id": list,
      "poster_id": this.puid,
      "commenter_id": this.cid,
      "like": "0",
      "share": "",
      "comment": "",
      "dislike": "",
      "view": "",
      "devicetype": "web"
    }

    console.log(this.feedlikedata);
    // this.SpinnerService.show();
    this.restservice.feedcomment(this.feedlikedata).subscribe((list: any) => {
      console.log(list);
      console.log(list.message);
      console.log(list.status);
      this.likestatus = list.message;
      this.toastr.warning('', list.message,{
        positionClass: 'toast-top-center'
   });
      this.getuserfeed();
      // this.SpinnerService.hide();
    });
  }
  feeddisdislikeslikeslistblack(list: any) {
    console.log(list);
    console.log("feeddisdislikeslikeslistblack");
    this.feedlikedata = {
      "feed_id": list,
      "poster_id": this.puid,
      "commenter_id": this.cid,
      "like": "",
      "share": "",
      "comment": "",
      "dislike": "1",
      "view": "",
      "devicetype": "web"
    }
    console.log(this.feedlikedata);
    // this.SpinnerService.show();
    this.restservice.feedcomment(this.feedlikedata).subscribe((list: any) => {
      console.log(list);
      console.log(list.message);
      console.log(list.status);
      this.likestatus = list.message;
      this.toastr.warning('', list.message,{
        positionClass: 'toast-top-center'
   });
      this.getuserfeed();
      // this.SpinnerService.hide();
    });
  }
  feeddisdislikeslikeslistcolor(list: any) {
    console.log(list);
    console.log("feeddisdislikeslikeslistcolor");
    this.feedlikedata = {
      "feed_id": list,
      "poster_id": this.puid,
      "commenter_id": this.cid,
      "like": "",
      "share": "",
      "comment": "",
      "dislike": "0",
      "view": "",
      "devicetype": "web"
    }
    console.log(this.feedlikedata);
    // this.SpinnerService.show();
    this.restservice.feedcomment(this.feedlikedata).subscribe((list: any) => {
      console.log(list);
      console.log(list.message);
      console.log(list.status);
      this.likestatus = list.message;
      this.toastr.warning('', list.message,{
        positionClass: 'toast-top-center'
   });
      this.getuserfeed();
      // this.SpinnerService.hide();
    });
  }
  commentsubmit() {
    this.submitted = true;
    this.feedcommentmessage = {
      "feed_id": this.sidfeedcomment,
      "poster_id": this.puid,
      "commenter_id": this.cid,
      "like": "",
      "share": "",
      "comment": this.commentForm.value.comment,
      "dislike": "",
      "view": "",
      "devicetype": "web"
    }
    console.log(this.feedcommentmessage);
    this.restservice.feedcomment(this.feedcommentmessage).subscribe((list: any) => {
      console.log(list.message);
      this.toastr.warning('', list.message,{
        positionClass: 'toast-top-center'
   });
      this.commentForm.controls['comment'].reset();
      this.closecommentmodel.nativeElement.click();
      this.getuserfeed();
    });
  }
 

  follow(){
    console.log("click");
    this.followusermodel = {
      "uid": this.userid,
      "fid": this.user_id
    }
    console.log(this.followusermodel);
     this.SpinnerService.show();
    this.restservice.sendfriendrequest(this.followusermodel).subscribe((list: any) => {
      console.log(list);
      this.SpinnerService.hide();
      this.toastr.warning('', list.message,{
        positionClass: 'toast-top-center'
   });
    });
    this.router.navigate( ['followingProfile'], { queryParams: this.userData, skipLocationChange: true });
 // this.router.navigate( ['followProfile'], { queryParams: this.searchlist[0], skipLocationChange: true});
}

followerslistshow(){
  console.log("click");
  this.followerlistmodel = {
    "uid": this.user_id
  }
  console.log(this.followerlistmodel);
  this.restservice.followerList(this.followerlistmodel).subscribe((list: any) => {
    console.log(list.message);
  this.viewfollowerslist =list.data;
  });
}

followersnavigate(id: any){
  console.log(id);
  this.searchpassdata = [ {
       user_id:id
       }];
 this.router.navigate(['followingProfile'], { queryParams: this.searchpassdata[0]});
}

fullviewclick(p: any){
  console.log(p);
  const type ="image";
   this.fullviewclickdata =p;
    this.fullviewclickdatatype =type;
      console.log("fullviewclickdata",this.fullviewclickdata);
    // this.router.navigate(['fullview'], { queryParams: { id: p, type: type, skipLocationChange: true } });
        this.imagepopupModel.nativeElement.click();
}

fullviewclick1(p: any){
  console.log(p);
  const type ="video";
  this.fullviewclickdata =p;
    this.fullviewclickdatatype =type;
      console.log("fullviewclickdata",this.fullviewclickdata);
    // this.router.navigate(['fullview'], { queryParams: { id: p, type: type, skipLocationChange: true } });
        this.imagepopupModel.nativeElement.click();
}

}