import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FollowprofileComponent } from './followprofile.component';

describe('FollowprofileComponent', () => {
  let component: FollowprofileComponent;
  let fixture: ComponentFixture<FollowprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FollowprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FollowprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
