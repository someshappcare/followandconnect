import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConstImages } from 'src/app/helpers/const-images';

@Component({
  selector: 'app-fullview',
  templateUrl: './fullview.component.html',
  styleUrls: ['./fullview.component.css']
})
export class FullviewComponent implements OnInit {
id: any;
data: any; type: any;
arrowright = ConstImages.arrowright;
arrowleft = ConstImages.arrowleft;
  constructor( private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(
      params => {
       this.data =params.id;
       this.type =params.type;
       console.log("array",this.data);
       console.log("type",this.type );

      });
  }

}
