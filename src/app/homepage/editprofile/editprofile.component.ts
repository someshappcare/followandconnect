import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ConstImages } from 'src/app/helpers/const-images';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.css']
})
export class EditprofileComponent implements OnInit {

  left_image = ConstImages.left_image;
  logo = ConstImages.logo;
  india = ConstImages.inida;
  usa = ConstImages.usa;
  canada = ConstImages.canada;
  germany = ConstImages.germany;
  singapore = ConstImages.singapore;
  usernameValidation = ConstImages.username_validation;
  passwordValidation = ConstImages.password_validation;
  emailValidation = ConstImages.email_validation;
  emailpatternValidation = ConstImages.emailpattern_validation;
  userIdValidation = ConstImages.userId_validation;
  confirmpasswordValidation = ConstImages.confirmpassword_validation;
  confirmpasswordvalidatorValidation = ConstImages.confirmpasswordvalidator_validation;
  mobilenoValidation = ConstImages.mobileno_validation;
  mobilenolengthValidation = ConstImages.mobilenolength_validation;
  mobilenodigitsValidation = ConstImages.mobilenodigits_validation;
  dobValidation = ConstImages.dob_validation;
  genderValidation = ConstImages.gender_validation;
  countryValidation = ConstImages.country_validation;
  defaultpic = ConstImages.defaultpic;
  // userDetailsbyID= [];
  userDetailsbyID: any;
  editForm: FormGroup;
  submitted = false;
  error = '';
  selectedfile: any;
  maxDate = new Date().toISOString().split("T")[0];
  userprofileId;
  format: any;
  fileimage: any;
  url: any;
  uploadprofile: any;
  profilepic;
  selectedcountry =""; initialCountry ="";
  countryflagvalue: any;
  userusername: any; useremail: any; useruser_id: any; usermobile; userpassword; usercountry; userdob; usergender;
  constructor(private toastr: ToastrService, private formBuilder: FormBuilder, private router: Router, private restservice: RestapiService, private SpinnerService: NgxSpinnerService) { }
  gender: any[] = [
    { value: '1', option: 'Male' },
    { value: '2', option: 'Female' },
    { value: '3', option: 'Others' }
  ];

  userdetails = JSON.parse(localStorage.getItem("currentUser"));
  userid = this.userdetails[0].user_id;
  ngOnInit() {
    this.userprofileId = {
      user_id: this.userdetails[0].user_id
    }
    this.getuser();
  }

  get f() { return this.editForm.controls; }

  getuser() {
    this.SpinnerService.show();
    this.restservice.getuserProfile(this.userprofileId).subscribe((list: any) => {
      this.userDetailsbyID = list.data;
      console.log(this.userDetailsbyID);
      console.log(this.userDetailsbyID[0].CountryCode);
      this.selectedcountry=this.userDetailsbyID[0].country;
      this.countryflagvalue =this.userDetailsbyID[0].CountryCode;
      this.profilepic = this.userDetailsbyID[0].profile_pic;
      this.editForm = this.formBuilder.group({
        fullname: [this.userDetailsbyID[0].fullname, [Validators.required]],
        email: [this.userDetailsbyID[0].email, [Validators.required, Validators.email]],
        userid: [this.userDetailsbyID[0].username, [Validators.required]],
        dob: [this.userDetailsbyID[0].dob, [Validators.required]],
        flag: [this.countryflagvalue, []],
        mobile: [this.userDetailsbyID[0].mobile, [Validators.required, Validators.pattern("^[0-9]*$")]],
        gender: [this.userDetailsbyID[0].gender, [Validators.required]],
        country: [this.userDetailsbyID[0].country, [Validators.required]]
      });
      this.SpinnerService.hide();
    });
  }

  onSubmit() {
    const editObj = {
      "fullname": this.editForm.value.fullname,
      "username": this.editForm.value.user_id,
      "email": this.editForm.value.email,
      "mobile": this.editForm.value.mobile,
      "gender": this.editForm.value.gender,
      "dob":this.editForm.value.dob,
      "password": this.editForm.value.password,
      "country": this.editForm.value.country,
      "deviceid": "151513",
      "devicetype": "web",
      "devicetoken": "web",
      "flag": '',
      "API-KEY": ConstImages.apikey,
      "user_id": this.userid,
      "CountryCode": this.countryflagvalue
    }
    this.submitted = true;
    console.log(this.editForm.value);
    if (this.editForm.invalid) {
      return;
    }
    console.log(editObj);
    this.SpinnerService.show();
    this.restservice.editUserProfile(editObj).subscribe((list: any) => {
      if (list.status == true) {
        console.log(list.message);
        // this.error = list.message;
        this.SpinnerService.hide();
        this.toastr.warning('', list.message,{
          positionClass: 'toast-top-center'
     });
        this.router.navigate(['/userprofile']);
      } else {
        console.log(list.message);
        this.error = list.message;
        this.SpinnerService.hide();
      }
    }, err => {
      this.SpinnerService.hide();
      console.log("error message");
    });
  }
  onSelectFile(event) {
    this.selectedfile = event.target.files && event.target.files[0];
    if (this.selectedfile) {
      var reader = new FileReader();
      reader.readAsDataURL(this.selectedfile);
      console.log(this.selectedfile, this.selectedfile.name, this.selectedfile.type);
      if (this.selectedfile.type.indexOf("image") > -1) {
        this.format = "image";
        this.fileimage = this.selectedfile;
      }
      reader.onload = event => {
        this.url = (<FileReader>event.target).result;
      };
    }
    this.uploadprofilepic();
  }

  uploadprofilepic() {
    console.log("upload profile pic");
    const formData = new FormData();
    formData.append('user_id', this.userid);
    formData.append('profile_pic', this.fileimage);

    console.log(formData);
    console.log(this.userid);
    this.SpinnerService.show();
    this.restservice.profilephoto_update(formData).subscribe((list: any) => {
      console.log(list.message);
      console.log(list);
      this.SpinnerService.hide();
      this.router.navigate(['/userprofile']);
    });
  }

  onCountryChange(event: any){
    console.log(event.dialCode);
    this.countryflagvalue ="+"+event.dialCode;
     this.selectedcountry = event.name.split(' ', 1);
     this.selectedcountry =this.selectedcountry[0];
    console.log(this.countryflagvalue,this.selectedcountry);
    console.log(event.iso2);
  } 
}