import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-intotopublic',
  templateUrl: './intotopublic.component.html',
  styleUrls: ['./intotopublic.component.css']
})
export class IntotopublicComponent implements OnInit {
  viewintotolist: [];
  userdetails: any;
  userid: any;
  intoto: any; image: any; finalimg: any; finalimage: any;
  element: any; error: any; video: any; finalvid: any; finalvideo: any;
  constructor(private restservice: RestapiService, private SpinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
    this.image ='';
    this.video ='';
    this.userdetails = JSON.parse(localStorage.getItem("currentUser"));
    this.userid = this.userdetails[0].user_id;
    this.getintotolist();
  }

  getintotolist() {
    this.intoto = {
      "uid": this.userid
    }
    this.SpinnerService.show();
    this.restservice.getIntoto(this.intoto).subscribe((list: any) => {
      console.log(list);
      this.viewintotolist = list.data;
     for (var i =0; i < list.data.length; i++) {
      if(list.data[i].imgf && list.data[i].feed_list.prvcy=='Public'){
       this.image +=list.data[i].imgf +",";
       console.log("image",this.image) ;
      }
      if(list.data[i].vf && list.data[i].feed_list.prvcy=='Public'){
        this.video +=list.data[i].vf +",";;
       }
      }
      this.finalimg = this.image.split(',');
      this.finalimage =this.finalimg[0];
     this.finalvid = this.video.split(',');
     this.finalvideo =this.finalvid[0];
     console.log("finalvideo",this.finalvideo);
     console.log("finalimage",this.finalimage);
     this.SpinnerService.hide();
      });
  }
}

