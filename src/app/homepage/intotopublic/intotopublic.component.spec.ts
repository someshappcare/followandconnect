import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntotopublicComponent } from './intotopublic.component';

describe('IntotopublicComponent', () => {
  let component: IntotopublicComponent;
  let fixture: ComponentFixture<IntotopublicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntotopublicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntotopublicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
