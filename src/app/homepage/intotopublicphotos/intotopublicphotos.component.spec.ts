import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntotopublicphotosComponent } from './intotopublicphotos.component';

describe('IntotopublicphotosComponent', () => {
  let component: IntotopublicphotosComponent;
  let fixture: ComponentFixture<IntotopublicphotosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntotopublicphotosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntotopublicphotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
