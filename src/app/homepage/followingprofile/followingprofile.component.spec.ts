import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FollowingprofileComponent } from './followingprofile.component';

describe('FollowingprofileComponent', () => {
  let component: FollowingprofileComponent;
  let fixture: ComponentFixture<FollowingprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FollowingprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FollowingprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
