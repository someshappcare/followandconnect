import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FriendsprofileComponent } from './friendsprofile.component';

describe('FriendsprofileComponent', () => {
  let component: FriendsprofileComponent;
  let fixture: ComponentFixture<FriendsprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FriendsprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FriendsprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
