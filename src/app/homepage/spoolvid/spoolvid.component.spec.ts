import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpoolvidComponent } from './spoolvid.component';

describe('SpoolvidComponent', () => {
  let component: SpoolvidComponent;
  let fixture: ComponentFixture<SpoolvidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoolvidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoolvidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
