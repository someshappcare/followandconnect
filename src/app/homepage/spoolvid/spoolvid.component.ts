import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ConstImages } from 'src/app/helpers/const-images';
import { RestapiService } from 'src/app/restapi.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-spoolvid',
  templateUrl: './spoolvid.component.html',
  styleUrls: ['./spoolvid.component.css']
})
export class SpoolvidComponent implements OnInit {

  left_image = ConstImages.left_image;
  like = ConstImages.like;
  likeColor = ConstImages.likeColor;
  dislike = ConstImages.dislike;
  dislikeColor = ConstImages.dislikeColor;
  add = ConstImages.add;
  eye = ConstImages.eye;
  comment = ConstImages.comment;
  sideDots = ConstImages.side_dots;
  profile = ConstImages.profile;
  defaultpic = ConstImages.defaultpic;
  upload_cross = ConstImages.upload_cross;
  commentForm: FormGroup;
  editfeedform: FormGroup;
  submitted = false;
  uploadform: FormGroup;
  url: any;
  format: any;
  imageName: any;
  imageType: any;
  feedpostdata: any;
  feedgetdata: any;
  userdetails: any;
  feedcommentdata: any;
  feedcommentmessage: any;
  userid: any; token: any;
  uploadusername: any;
  uploadfullname: any;
  viewlist: [];
  commentlist: [];
  sid: any; puid: any; cid: any;
  filevideo: any;
  selectedfile: any;
  uploadprofile_pic: any;
  deletefeedmodel: any;
  editfeeddescription: any;
  videostofile = []; editvideos = [];
  editvideostext = []; indexselectedvideos: any; feedlikedata: any; sidfeedcomment: any; likestatus: any;
  @ViewChild('closeuploadmodel') closeuploadmodel;
  @ViewChild('closecommentmodel') closecommentmodel;
  @ViewChild('closeeditmodel') closeeditmodel;
  viewfeedmodel: any; image: any;
  editfeeddetailslist: any;
  public element;
  public error; mydata = [];
  myvideoFiles: string[] = [];
   @ViewChild('imagepopupModel') imagepopupModel: any;
arrowright = ConstImages.arrowright;
arrowleft = ConstImages.arrowleft;
fullviewclickdata: any;
  fullviewclickdatatype: any;
  constructor(private http: HttpClient, private router: Router, private toastr: ToastrService, private formBuilder: FormBuilder, private restservice: RestapiService, private SpinnerService: NgxSpinnerService) { }

  category: any[] = [
    { value: '0', option: 'Private' },
    { value: '1', option: 'Public' }
  ];
  selectedcategory = 'public';
  ngOnInit() {
    this.uploadform = this.formBuilder.group({
      category: ['', []],
      description: ['', []]
    });

    this.editfeedform = this.formBuilder.group({
      editcategory: ['', []],
      editdescription: ['', []]
    });

    this.commentForm = this.formBuilder.group({
      comment: ['', []]
    });

    console.log(localStorage.getItem('currentUser'));
    this.userdetails = JSON.parse(localStorage.getItem("currentUser"));
    this.userid = this.userdetails[0].user_id;
    this.uploadusername = this.userdetails[0].username;
    this.uploadfullname = this.userdetails[0].fullname;
    this.uploadprofile_pic = this.userdetails[0].profile_pic;
    this.token = this.userdetails[0].devicetoken;
    console.log(this.userid);
    this.getfeed();
  }


  getfeed() {
    this.feedgetdata = {
      "uid": this.userid
    }
    this.SpinnerService.show();
    this.restservice.spoolviddisplay(this.feedgetdata).subscribe((list: any) => {
      console.log(list);
      this.viewlist = list.data;
      if (list.data.length > 0) {
        this.puid = list.data[0].feed_list.puid;
        this.cid = list.data[0].feed_list.puid;
      }
      this.SpinnerService.hide();
    });
  }

  feedcommentlist(list: any) {
    this.sidfeedcomment = list;
    console.log("feedcommentlist");
    console.log(this.sidfeedcomment);
    this.feedcommentdata = {
      "feed_id": this.sidfeedcomment,
    }
    this.SpinnerService.show();
    this.restservice.feedcommentList(this.feedcommentdata).subscribe((list: any) => {
      console.log(list);
      this.commentlist = list.data;
      console.log(this.commentlist);
      this.SpinnerService.hide();
    });
  }
  feedlikeslist(list: any) {
    console.log(list);
    this.feedlikedata = {
      "feed_id": list,
      "poster_id": this.puid,
      "commenter_id": this.cid,
      "like": "1",
      "share": "",
      "comment": "",
      "dislike": "",
      "view": "",
      "devicetoken": this.token
    }
    console.log(this.feedlikedata);
    // this.SpinnerService.show();
    this.restservice.feedcomment(this.feedlikedata).subscribe((list: any) => {
      console.log(list);
      console.log(list.message);
      console.log(list.status);
      this.likestatus = list.message;
      this.toastr.warning('', list.message, {
        positionClass: 'toast-top-center'
      });
      this.getfeed();
      // this.SpinnerService.hide();
    });
  }
  feeddislikeslikeslist(list: any) {
    console.log(list);
    this.feedlikedata = {
      "feed_id": list,
      "poster_id": this.puid,
      "commenter_id": this.cid,
      "like": "0",
      "share": "",
      "comment": "",
      "dislike": "",
      "view": "",
      "devicetoken": this.token
    }

    console.log(this.feedlikedata);
    // this.SpinnerService.show();
    this.restservice.feedcomment(this.feedlikedata).subscribe((list: any) => {
      console.log(list);
      console.log(list.message);
      console.log(list.status);
      this.likestatus = list.message;
      this.toastr.warning('', list.message, {
        positionClass: 'toast-top-center'
      });
      this.getfeed();
      // this.SpinnerService.hide();
    });
  }
  feeddisdislikeslikeslistblack(list: any) {
    console.log(list);
    console.log("feeddisdislikeslikeslistblack");
    this.feedlikedata = {
      "feed_id": list,
      "poster_id": this.puid,
      "commenter_id": this.cid,
      "like": "",
      "share": "",
      "comment": "",
      "dislike": "1",
      "view": "",
      "devicetoken": this.token
    }
    console.log(this.feedlikedata);
    // this.SpinnerService.show();
    this.restservice.feedcomment(this.feedlikedata).subscribe((list: any) => {
      console.log(list);
      console.log(list.message);
      console.log(list.status);
      this.likestatus = list.message;
      this.toastr.warning('', list.message, {
        positionClass: 'toast-top-center'
      });
      this.getfeed();
      // this.SpinnerService.hide();
    });
  }
  feeddisdislikeslikeslistcolor(list: any) {
    console.log(list);
    console.log("feeddisdislikeslikeslistcolor");
    this.feedlikedata = {
      "feed_id": list,
      "poster_id": this.puid,
      "commenter_id": this.cid,
      "like": "",
      "share": "",
      "comment": "",
      "dislike": "0",
      "view": "",
      "devicetoken": this.token
    }
    console.log(this.feedlikedata);
    // this.SpinnerService.show();
    this.restservice.feedcomment(this.feedlikedata).subscribe((list: any) => {
      console.log(list);
      console.log(list.message);
      console.log(list.status);
      this.likestatus = list.message;
      this.toastr.warning('', list.message, {
        positionClass: 'toast-top-center'
      });
      this.getfeed();
      // this.SpinnerService.hide();
    });
  }
  commentsubmit() {
    this.submitted = true;
    this.feedcommentmessage = {
      "feed_id": this.sidfeedcomment,
      "poster_id": this.puid,
      "commenter_id": this.cid,
      "like": "",
      "share": "",
      "comment": this.commentForm.value.comment,
      "dislike": "",
      "view": "",
      "devicetoken": this.token
    }
    console.log(this.feedcommentmessage);
    this.restservice.feedcomment(this.feedcommentmessage).subscribe((list: any) => {
      console.log(list.message);
      this.toastr.warning('', list.message, {
        positionClass: 'toast-top-center'
      });
      this.commentForm.controls['comment'].reset();
      this.closecommentmodel.nativeElement.click();
      this.getfeed();
    });
  }

  uploadsubmit() {
    console.log("upload");
    console.log(this.uploadform.value);
    const formData = new FormData();
    formData.append('uid', this.userid);
    formData.append('API-KEY', ConstImages.apikey);
    formData.append('feedtext', this.uploadform.value.description);
    formData.append('privicy', this.uploadform.value.category);
    formData.append('address', 'hyderabad');
    formData.append('ltd', '17.26');
    formData.append('lng', '78.25');
    formData.append('isspoolvid', 'Yes');
    // formData.append('videos[]', this.filevideo);
    for (var i = 0; i < this.myvideoFiles.length; i++) {
      formData.append('videos[]', this.myvideoFiles[i]);
    }

    console.log(formData);
    console.log(this.userid);
    if (this.myvideoFiles.length > 0) {
      this.SpinnerService.show();
      this.restservice.feedpost(formData).subscribe((list: any) => {
        console.log(list.message);
        this.getfeed();
        this.mydata = [];
        this.myvideoFiles = [];
        this.uploadform.controls['description'].reset();
        this.closeuploadmodel.nativeElement.click();
        this.SpinnerService.hide();
      });
    }
  }

  onSelectFile(event: any) {
    const files = event.target.files;
    if (files) {
      for (const file of files) {
        const reader = new FileReader();
        reader.onload = (e: any) => {
          if (file.type.indexOf("video") > -1) {
            this.mydata.push({
              url: e.target.result,
              type: 'video'
            });
            console.log("video");
            this.myvideoFiles.push(file);
            console.log("loop", this.myvideoFiles);

          }
        };
        reader.readAsDataURL(file);
      }
    }
  }

  deletefeeddetails(id: any, feedid: any) {
    console.log(id, feedid);
    this.deletefeedmodel = {
      "uid": id,
      "feed_id": feedid
    }
  }
  deletefeed() {
    console.log(this.deletefeedmodel);
    this.restservice.deleteuserfeed(this.deletefeedmodel).subscribe((list: any) => {
      console.log(list.message);
      console.log(list);
      this.getfeed();
    });
  }

  editUserdetails(view: any) {
    console.log(view.user_id);
    console.log("editfeed", view)
    this.editfeeddetailslist = view;
    this.editfeeddescription = this.editfeeddetailslist.feed_list.feed;
    this.editvideos = this.editfeeddetailslist.vf?.split(',');
    this.editvideostext = this.editfeeddetailslist.feed_list.vf?.split(',');
    this.indexselectedvideos = this.editvideos.length;
    console.log(this.editfeeddetailslist.feed_list.sid, this.editfeeddescription, this.editvideos, this.editvideos, this.editvideostext);
    if (this.editfeeddetailslist.vf != '') {
      for (var i = 0; i < this.editvideos.length; i++) {
        this.getImage(this.editvideos[i], this.editvideostext[i]).subscribe((response: any) => {
          this.videostofile.push(response);
          this.myvideoFiles.push(response);
          console.log(this.videostofile, response);
        }
        );
      }
    }
  }
  getImage(i: any, text: any) {
    return this.http
      .get(i, {
        responseType: "arraybuffer"
      })
      .pipe(
        map(response => {
          return new File([response], text);
        })
      );
  }


  editfeedsubmit() {
    const editformData = new FormData();
    editformData.append('uid', this.userid);
    // editformData.append('API-KEY', ConstImages.apikey);
    editformData.append('feedtext', this.editfeedform.value.editdescription);
    editformData.append('privicy', this.editfeedform.value.editcategory);
    editformData.append('address', 'hyderabad');
    editformData.append('ltd', '17.26');
    editformData.append('lng', '78.25');
    editformData.append('isspoolvid', 'Yes');
    for (var i = 0; i < this.myvideoFiles.length; i++) {
      editformData.append('videos[]', this.myvideoFiles[i]);
    }
    editformData.append('feed_id', this.editfeeddetailslist.feed_list.sid);
    console.log(this.editfeedform.value);
    console.log(editformData);
    if (this.myvideoFiles.length > 0) {
      this.SpinnerService.show();
      this.restservice.edituserfeed(editformData).subscribe((list: any) => {
        this.toastr.warning('', list.message, {
          positionClass: 'toast-top-center'
        });
        this.mydata = [];
        this.myvideoFiles = [];
        this.getfeed();
        this.closeeditmodel.nativeElement.click();
        this.SpinnerService.hide();
      });
    }
  }

  fullviewclick1(p: any, feedid: any) {
    console.log("fullviewclick", p, "feedid", feedid);
    const type = "video";
    this.viewfeedmodel = {
      "uid": this.userid,
      "feed_id": feedid
    }
    console.log(this.viewfeedmodel);
    this.restservice.viewcount(this.viewfeedmodel).subscribe((list: any) => {
      console.log(list.message);
      console.log(list);
    });
    this.fullviewclickdata =p;
    this.fullviewclickdatatype =type;
      console.log("fullviewclickdata",this.fullviewclickdata);
    // this.router.navigate(['fullview'], { queryParams: { id: p, type: type, skipLocationChange: true } });
        this.imagepopupModel.nativeElement.click();
  }

  sharefeeddetails(video: any) {
    this.image = video.split(',').join("\n");
    console.log(this.image);
  }

  removevideosSelectedFile(index: any) {
    this.mydata.splice(index, 1);
    this.myvideoFiles.splice(index, 1);
    console.log("removeSelectedFile", this.mydata, this.myvideoFiles);
  }

  removeeditvideoSelectedFile(index: any) {
    this.editvideos.splice(index, 1);
    this.myvideoFiles.splice(index, 1);
    this.indexselectedvideos = this.editvideos.length;
    console.log("removeeditvideoSelectedFile", this.editvideos);
  }
  removeeditvideosuploadSelectedFile(index: any) {
    this.mydata.splice(index, 1);
    this.myvideoFiles.splice(this.indexselectedvideos + index, 1);
    console.log("removeeditvideoSelectedFile", this.mydata, this.myvideoFiles);
  }

}