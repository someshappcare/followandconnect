import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntotoprivatevideosComponent } from './intotoprivatevideos.component';

describe('IntotoprivatevideosComponent', () => {
  let component: IntotoprivatevideosComponent;
  let fixture: ComponentFixture<IntotoprivatevideosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntotoprivatevideosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntotoprivatevideosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
