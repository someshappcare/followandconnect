import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';
import { ConstImages } from 'src/app/helpers/const-images';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit {

  left_image = ConstImages.left_image;
  like = ConstImages.like;
  likeColor = ConstImages.likeColor;
  dislike = ConstImages.dislike;
  dislikeColor = ConstImages.dislikeColor;
  add = ConstImages.add;
  eye = ConstImages.eye;
  comment = ConstImages.comment;
  sideDots = ConstImages.side_dots;
  userprofile = ConstImages.userprofile;
  upload_cross = ConstImages.upload_cross;
  friends = ConstImages.friends;
  post = ConstImages.post;
  userlike = ConstImages.userlike;
  search = ConstImages.search;
  profile = ConstImages.profile;
  defaultpic = ConstImages.defaultpic;
  friendscount: any;
  followerscount: any;
  postscount: any;
  myviewForm: FormGroup;
  submitted = false;
  searchedKeywordforfriends: string;
  searchedKeywordforfollowers: string;
  commentForm: FormGroup;
  url: any;
  format: any;
  imageName: any; editfeeddescription: any; indexselected: any;
  imageType: any;
  feedpostdata: any;
  feedgetdata: any;
  feedcommentdata: any;
  feedcommentmessage: any;
  frindslistmodel: any;
  followerlistmodel: any;
  deletefeedmodel: any;
  editfeedmodel: any;
  editfeeddetailslist: any;
  userid: any;
  editfeedform: FormGroup;
  username: any;
  fullname: any; token: any;
  userdetails: any;
  viewlist: [];
  commentlist: [];
  friendslist = [];
  viewfriendslist = [];
  viewfollowerslist = [];
  imagestofile = []; videostofile = []; editimages = []; editimagestext = []; editvideos = [];
  editvideostext = []; indexselectedvideos: any;
  sid: any; puid: any; cid: any; profilepic: any;
  sidfeedcomment: any;
  feedlikedata: any;
  element: any; error: any;
  likestatus: any;
  searchpassdata: any;
  frndsearchpassdata: any;
  mydata = []; image: any; video: any;
  myimageFiles: string[] = [];
  myvideoFiles: string[] = [];
  @ViewChild('closecommentmodel') closecommentmodel;
  @ViewChild('closefollowerssmodel') closefollowerssmodel;
  @ViewChild('closefriendsmodel') closefriendsmodel;
  @ViewChild('closeeditmodel') closeeditmodel;
  @ViewChild('closedropdown') closedropdown;
   @ViewChild('imagepopupModel') imagepopupModel: any;
arrowright = ConstImages.arrowright;
arrowleft = ConstImages.arrowleft;
fullviewclickdata: any;
  fullviewclickdatatype: any;
  constructor(private http: HttpClient, private router: Router, private toastr: ToastrService, private formBuilder: FormBuilder, private restservice: RestapiService, private SpinnerService: NgxSpinnerService) { }
  category: any[] = [
    { value: '0', option: 'Private' },
    { value: '1', option: 'Public' }
  ];
  selectedcategory = 'Public';
  ngOnInit() {
    this.editfeedform = this.formBuilder.group({
      editcategory: ['', []],
      editdescription: ['', []]
    });

    this.commentForm = this.formBuilder.group({
      comment: ['', []]
    });
    console.log(localStorage.getItem('currentUser'));
    this.userdetails = JSON.parse(localStorage.getItem("currentUser"));
    this.userid = this.userdetails[0].user_id;
    this.token = this.userdetails[0].devicetoken;
    this.getuserfeed();
  }

  getuserfeed() {
    this.feedgetdata = {
      "uid": this.userid
    }
    this.SpinnerService.show();
    this.restservice.getuserprofilefeed(this.feedgetdata).subscribe((list: any) => {
      console.log(list);
      this.viewlist = list.data;
      this.fullname = list.user_info[0].fullname;
      console.log(list.user_info[0].fullname);
      this.username = list.user_info[0].username;
      this.profilepic = list.user_info[0].profile_pic;
      if (list.data.length > 0) {
        this.puid = list.data[0].feed_list.puid;
        this.cid = list.data[0].feed_list.puid;
      }
      this.postscount = list.posts;
      this.friendscount = list.friends;
      this.followerscount = list.followers;
      this.SpinnerService.hide();
    });
  }

  feedcommentlist(list: any) {
    this.sidfeedcomment = list;
    console.log("feedcommentlist");
    console.log(this.sidfeedcomment);
    this.feedcommentdata = {
      "feed_id": this.sidfeedcomment,
    }
    this.SpinnerService.show();
    this.restservice.feedcommentList(this.feedcommentdata).subscribe((list: any) => {
      console.log(list);
      this.commentlist = list.data;
      console.log(this.commentlist);
      this.SpinnerService.hide();
    });
  }
  feedlikeslist(list: any) {
    console.log(list);
    this.feedlikedata = {
      "feed_id": list,
      "poster_id": this.puid,
      "commenter_id": this.cid,
      "like": "1",
      "share": "",
      "comment": "",
      "dislike": "",
      "view": "",
      "devicetype": "web"
    }
    console.log(this.feedlikedata);
    // this.SpinnerService.show();
    this.restservice.feedcomment(this.feedlikedata).subscribe((list: any) => {
      console.log(list);
      console.log(list.message);
      console.log(list.status);
      this.likestatus = list.message;
      this.toastr.warning('', list.message, {
        positionClass: 'toast-top-center'
      });
      this.getuserfeed();
      // this.SpinnerService.hide();
    });
  }
  feeddislikeslikeslist(list: any) {
    console.log(list);
    this.feedlikedata = {
      "feed_id": list,
      "poster_id": this.puid,
      "commenter_id": this.cid,
      "like": "0",
      "share": "",
      "comment": "",
      "dislike": "",
      "view": "",
      "devicetype": "web"
    }

    console.log(this.feedlikedata);
    // this.SpinnerService.show();
    this.restservice.feedcomment(this.feedlikedata).subscribe((list: any) => {
      console.log(list);
      console.log(list.message);
      console.log(list.status);
      this.likestatus = list.message;
      this.toastr.warning('', list.message, {
        positionClass: 'toast-top-center'
      });
      this.getuserfeed();
      // this.SpinnerService.hide();
    });
  }
  feeddisdislikeslikeslistblack(list: any) {
    console.log(list);
    console.log("feeddisdislikeslikeslistblack");
    this.feedlikedata = {
      "feed_id": list,
      "poster_id": this.puid,
      "commenter_id": this.cid,
      "like": "",
      "share": "",
      "comment": "",
      "dislike": "1",
      "view": "",
      "devicetype": "web"
    }
    console.log(this.feedlikedata);
    // this.SpinnerService.show();
    this.restservice.feedcomment(this.feedlikedata).subscribe((list: any) => {
      console.log(list);
      console.log(list.message);
      console.log(list.status);
      this.likestatus = list.message;
      this.toastr.warning('', list.message, {
        positionClass: 'toast-top-center'
      });
      this.getuserfeed();
      // this.SpinnerService.hide();
    });
  }
  feeddisdislikeslikeslistcolor(list: any) {
    console.log(list);
    console.log("feeddisdislikeslikeslistcolor");
    this.feedlikedata = {
      "feed_id": list,
      "poster_id": this.puid,
      "commenter_id": this.cid,
      "like": "",
      "share": "",
      "comment": "",
      "dislike": "0",
      "view": "",
      "devicetype": "web"
    }
    console.log(this.feedlikedata);
    // this.SpinnerService.show();
    this.restservice.feedcomment(this.feedlikedata).subscribe((list: any) => {
      console.log(list);
      console.log(list.message);
      console.log(list.status);
      this.likestatus = list.message;
      this.toastr.warning('', list.message, {
        positionClass: 'toast-top-center'
      });
      this.getuserfeed();
      // this.SpinnerService.hide();
    });
  }
  commentsubmit() {
    this.submitted = true;
    this.feedcommentmessage = {
      "feed_id": this.sidfeedcomment,
      "poster_id": this.puid,
      "commenter_id": this.cid,
      "like": "",
      "share": "",
      "comment": this.commentForm.value.comment,
      "dislike": "",
      "view": "",
      "devicetype": "web"
    }
    console.log(this.feedcommentmessage);
    this.restservice.feedcomment(this.feedcommentmessage).subscribe((list: any) => {
      console.log(list.message);
      this.toastr.warning('', list.message, {
        positionClass: 'toast-top-center'
      });
      this.commentForm.controls['comment'].reset();
      this.closecommentmodel.nativeElement.click();
      this.getuserfeed();
    });
  }

  friendslistshow() {
    console.log("click");
    this.frindslistmodel = {
      "uid": this.userid
    }
    console.log(this.frindslistmodel);
    this.restservice.friendsList(this.frindslistmodel).subscribe((list: any) => {
      console.log(list.data);
      this.viewfriendslist = list.data;
    });
  }

  followerslistshow() {
    console.log("click");
    this.followerlistmodel = {
      "uid": this.userid
    }
    console.log(this.followerlistmodel);
    this.restservice.followerList(this.followerlistmodel).subscribe((list: any) => {
      console.log(list.message);
      this.viewfollowerslist = list.data;
    });
  }

  friendsnavigate(id: any) {
    console.log(id);
    this.closefriendsmodel.nativeElement.click();
    this.frndsearchpassdata = [{
      user_id: id
    }];
    this.router.navigate(['friendsProfile'], { queryParams: this.frndsearchpassdata[0] });
  }

  followersnavigate(id: any) {
    this.closefollowerssmodel.nativeElement.click();
    console.log(id);
    this.searchpassdata = [{
      user_id: id
    }];
    this.router.navigate(['followingProfile'], { queryParams: this.searchpassdata[0] });
  }

  fullviewclick(p: any) {
    console.log(p);
    const type = "image";
     this.fullviewclickdata =p;
    this.fullviewclickdatatype =type;
      console.log("fullviewclickdata",this.fullviewclickdata);
    // this.router.navigate(['fullview'], { queryParams: { id: p, type: type, skipLocationChange: true } });
        this.imagepopupModel.nativeElement.click();
  }

  fullviewclick1(p: any) {
    console.log(p);
    const type = "video";
     this.fullviewclickdata =p;
    this.fullviewclickdatatype =type;
      console.log("fullviewclickdata",this.fullviewclickdata);
    // this.router.navigate(['fullview'], { queryParams: { id: p, type: type, skipLocationChange: true } });
        this.imagepopupModel.nativeElement.click();
  }

  sharefeeddetails(view) {
    if (view.imgf != '') {
      this.image = view.imgf.split(',').join("\n");
      console.log(this.image);
    } else {
      this.image = view.vf.split(',').join("\n");
      console.log(this.image);
    }
  }

  deletefeeddetails(id: any, feedid: any) {
    console.log(id, feedid);
    this.deletefeedmodel = {
      "uid": id,
      "feed_id": feedid
    }
  }

  deletefeed() {
    console.log(this.deletefeedmodel);
    this.restservice.deleteuserfeed(this.deletefeedmodel).subscribe((list: any) => {
      console.log(list.message);
      this.toastr.warning('', list.message, {
        positionClass: 'toast-top-center'
      });
      console.log(list);
      this.getuserfeed();
    });
  }

  editUserdetails(view: any) {
    console.log(view.user_id);
    console.log("editfeed", view)
    this.editfeeddetailslist = view;
    this.editfeeddescription = this.editfeeddetailslist.feed_list.feed;
    this.editimages = this.editfeeddetailslist.imgf?.split(',');
    this.editvideos = this.editfeeddetailslist.vf?.split(',');
    this.editimagestext = this.editfeeddetailslist.feed_list.imgf?.split(',');
    this.editvideostext = this.editfeeddetailslist.feed_list.vf?.split(',');
    console.log(view.user_id);
    this.indexselected = this.editimages.length;
    this.indexselectedvideos = this.editvideos.length;
    console.log(this.editfeeddetailslist.feed_list.sid, this.editfeeddescription, this.editimages, this.editvideos, this.editimagestext, this.editvideostext);
    if (this.editfeeddetailslist.imgf != '') {
      for (var i = 0; i < this.editimages.length; i++) {
        this.getImage(this.editimages[i], this.editimagestext[i]).subscribe((response: any) => {
          this.imagestofile.push(response);
          this.myimageFiles.push(response);
          console.log(this.imagestofile, response);
        }
        );
      }
    }
    if (this.editfeeddetailslist.vf != '') {
      for (var i = 0; i < this.editvideos.length; i++) {
        this.getImage(this.editvideos[i], this.editvideostext[i]).subscribe((response: any) => {
          this.videostofile.push(response);
          this.myvideoFiles.push(response);
          console.log(this.videostofile, response);
        }
        );
      }
    }
  }

  getImage(i: any, text: any) {
    return this.http
      .get(i, {
        responseType: "arraybuffer"
      })
      .pipe(
        map(response => {
          return new File([response], text);
        })
      );
  }


  editfeedsubmit() {
    const editformData = new FormData();
    editformData.append('uid', this.userid);
    editformData.append('API-KEY', ConstImages.apikey);
    editformData.append('feedtext', this.editfeedform.value.editdescription);
    if(this.editfeedform.value.editcategory==''){
      editformData.append('privicy', this.selectedcategory);
    }else{
    editformData.append('privicy', this.editfeedform.value.editcategory);
    }
    editformData.append('address', 'hyderabad');
    editformData.append('ltd', '17.26');
    editformData.append('lng', '78.25');
    editformData.append('isspoolvid', 'No');
    editformData.append('feed_id', this.editfeeddetailslist.feed_list.sid);
    for (var i = 0; i < this.myimageFiles.length; i++) {
      editformData.append("images[]", this.myimageFiles[i]);
      editformData.append('videos[]', "");
    }
    for (var i = 0; i < this.myvideoFiles.length; i++) {
      editformData.append("images[]", "");
      editformData.append('videos[]', this.myvideoFiles[i]);
    }
    console.log(this.editfeedform.value);
    console.log(editformData);
    if (this.myvideoFiles.length > 0 || this.myimageFiles.length > 0 || this.editfeedform.value.editdescription != '') {
      this.SpinnerService.show();
      this.restservice.edituserfeed(editformData).subscribe((list: any) => {
        console.log(list.message);
        this.toastr.warning('', list.message, {
          positionClass: 'toast-top-center'
        });
        console.log("after edit upload", this.myimageFiles, this.myvideoFiles, this.mydata);
        this.mydata = [];
        this.myimageFiles = [];
        this.myvideoFiles = [];
        this.getuserfeed();
        console.log("after getfeed edit upload", this.myimageFiles, this.myvideoFiles, this.mydata);
        // this.editfeedform.controls['editdescription'].reset();
        this.closeeditmodel.nativeElement.click();
        this.SpinnerService.hide();
      });
    }
  }
  onSelectFile(event: any) {
    const files = event.target.files;
    if (files) {
      for (const file of files) {
        const reader = new FileReader();
        reader.onload = (e: any) => {
          if (file.type.indexOf("image") > -1) {
            this.mydata.push({
              url: e.target.result,
              type: 'img'
            });
            console.log("image");
            console.log("mydata", this.mydata);
            this.myimageFiles.push(file);
            console.log("loop", this.myimageFiles);

          } else if (file.type.indexOf("video") > -1) {
            this.mydata.push({
              url: e.target.result,
              type: 'video'
            });
            console.log("video");
            this.myvideoFiles.push(file);
            console.log("loop", this.myvideoFiles);

          }
        };
        reader.readAsDataURL(file);
      }
    }
  }
  removeeditimageSelectedFile(index: any) {
    this.editimages.splice(index, 1);
    this.myimageFiles.splice(index, 1);
    this.indexselected = this.editimages.length;
    console.log("removeeditimageSelectedFile", this.editimages, this.myimageFiles, this.indexselected, index);
  }
  removeeditimageuploadSelectedFile(index: any) {
    this.mydata.splice(index, 1);
    this.myimageFiles.splice(this.indexselected + index, 1);
    console.log("removeeditimageuploadSelectedFile", this.mydata, this.myimageFiles, this.indexselected + index, this.editimages.length, index);
  }

  removeeditvideoSelectedFile(index: any) {
    this.editvideos.splice(index, 1);
    this.myvideoFiles.splice(index, 1);
    this.indexselectedvideos = this.editvideos.length;
    console.log("removeeditvideoSelectedFile", this.editvideos);
  }
  removeeditvideosuploadSelectedFile(index: any) {
    this.mydata.splice(index, 1);
    this.myvideoFiles.splice(this.indexselected + index, 1);
    console.log("removeeditvideoSelectedFile", this.mydata, this.myvideoFiles);
  }
}

