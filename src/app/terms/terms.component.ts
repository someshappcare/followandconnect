import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ConstImages } from '../helpers/const-images';
import { RestapiService } from '../restapi.service';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css']
})
export class TermsComponent implements OnInit {

  terms: [];
  left_image = ConstImages.left_image;
  logo = ConstImages.logo;
  constructor(private restservice: RestapiService, private SpinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
    this.SpinnerService.show();
    this.restservice.termsandconditions().subscribe((list: any) => {
      console.log(list);
      this.SpinnerService.hide();
      this.terms =list.data;
   
    },err => {
      this.SpinnerService.hide();
      console.log("error message"+err);
    });
  }
}
