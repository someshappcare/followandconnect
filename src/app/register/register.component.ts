import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ConstImages } from 'src/app/helpers/const-images';
import { Passwordmatcher } from '../helpers/passwordmatcher';
import { RestapiService } from '../restapi.service';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';
import { MessagingService } from '../messaging.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  error = ''; userId: any;
  countryflagvalue ="+91"; initialCountry ; selectedcountry="India";
  onchangename: any; onchangeemail: any; randomuserid: any;
  maxDate = new Date().toISOString().split("T")[0];
  fieldTextTypePassword;   message: any;
  confirm_text_password; token: any;
  // const images & validations
  left_image = ConstImages.left_image;
  logo = ConstImages.logo;
  india = ConstImages.inida;
  usa = ConstImages.usa;
  canada = ConstImages.canada;
  germany = ConstImages.germany;
  singapore = ConstImages.singapore;
  usernameValidation = ConstImages.username_validation;
  passwordValidation = ConstImages.password_validation;
  emailValidation = ConstImages.email_validation;
  emailpatternValidation = ConstImages.emailpattern_validation;
  userIdValidation = ConstImages.userId_validation;
  confirmpasswordValidation = ConstImages.confirmpassword_validation;
  confirmpasswordvalidatorValidation = ConstImages.confirmpasswordvalidator_validation;
  mobilenoValidation = ConstImages.mobileno_validation;
  mobilenolengthValidation = ConstImages.mobilenolength_validation;
  mobilenodigitsValidation = ConstImages.mobilenodigits_validation;
  dobValidation = ConstImages.dob_validation;
  genderValidation = ConstImages.gender_validation;
  countryValidation = ConstImages.country_validation;

  toggleFieldPassword() {
    this.fieldTextTypePassword = !this.fieldTextTypePassword;
  }

  toggleConfirmFieldPassword() {
    this.confirm_text_password = !this.confirm_text_password;
  }

  constructor(private messagingService: MessagingService, private toastr: ToastrService, private router: Router, private formBuilder: FormBuilder, private restservice: RestapiService, private SpinnerService: NgxSpinnerService) { }
  gender: any[] = [
    { value: '1', option: 'Male' },
    { value: '2', option: 'Female' },
    { value: '2', option: 'Others' }
  ];
 
  onChangeEvent1(event: any) {
    console.log(event.target.value);
    this.onchangename = event.target.value;
  }

  onChangeEvent2(event: any) {
    console.log(event.target.value);
    this.onchangeemail = event.target.value;
  }

  onFocusEvent(event: any) {
    console.log("click");
    const randomid = {
      "fullname": this.onchangename,
      "email": this.onchangeemail
    }

    console.log();
    this.restservice.randomUserId(randomid).subscribe((list: any) => {
      console.log(list.data);
      this.randomuserid = list.data;
      this.registerForm.get("username").patchValue(this.randomuserid);
    });
  }

  ngOnInit() {
    this.token = JSON.parse(localStorage.getItem("token"));
    if(this.token ==null){
      console.log("if token null", this.token);
    this.token = "123456";
    console.log("if token null set", this.token);
    }
    console.log("token",this.token);
    this.registerForm = this.formBuilder.group({
      fullname: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      username: [this.randomuserid, [Validators.required]],
      password: ['', [Validators.required]],
      confirm_password: ['', [Validators.required]],
      flag: [this.countryflagvalue, []],
      dob: ['', [Validators.required]],
      mobile: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      gender: ['', [Validators.required]],
      country: [this.selectedcountry, [Validators.required]]
    }, {
      validator: Passwordmatcher('password', 'confirm_password')
    });
  }

  get f() { return this.registerForm.controls; }
  onSubmit() {
    const regobj = {
      "fullname": this.registerForm.value.fullname,
      "email": this.registerForm.value.email,
      "username": this.registerForm.value.username,
      "mobile": this.registerForm.value.mobile,
      "gender": this.registerForm.value.gender,
      "dob":  this.registerForm.value.dob,
      "password": this.registerForm.value.password,
      "country": this.selectedcountry,
      "deviceid": '151513',
      "devicetype": 'web',
      "devicetoken": this.token,
      "flag": '',
      "API-KEY": ConstImages.apikey,
      "CountryCode": this.countryflagvalue,
      "ip": 'ip',
      "lat":'lat',
      "lng": 'lng',
      "address":'Localaddress'
    }
    this.submitted = true;
    console.log("form");
    console.log(regobj);
    if (this.registerForm.invalid) {
      console.log(this.registerForm.invalid);
      console.log("return");
      return;
    }
    console.log(regobj);
    this.SpinnerService.show();
    this.restservice.registerUser(regobj).subscribe((list: any) => {
      if (list.status == true) {
        console.log(list.message);
        this.error = list.message;
        localStorage.removeItem('token');
        this.SpinnerService.hide();
        this.toastr.warning('', list.message,{
          positionClass: 'toast-top-center'
     });
        this.router.navigate(['/login']);
        
      } else {
        console.log(list.message);
        this.toastr.warning('', list.message,{
          positionClass: 'toast-top-center'
     });
        this.SpinnerService.hide();
      }
    }, err => {
      console.log("error message");
    });
  }

  onCountryChange(event: any){
  console.log(event);
   this.countryflagvalue ="+"+event.dialCode;
   this.selectedcountry = event.name.split(' ', 1);
   this.selectedcountry =this.selectedcountry[0];
  console.log(this.countryflagvalue,this.selectedcountry);
  console.log(event.iso2);
} 
 
}
