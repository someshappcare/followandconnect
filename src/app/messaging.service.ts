import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { AngularFireAuth } from '@angular/fire/auth';
import { BehaviorSubject } from 'rxjs'
import { AngularFireDatabase } from '@angular/fire/database';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MessagingService {

 currentMessage = new BehaviorSubject(null);
constructor( private angularFireDB: AngularFireDatabase, private angularFireAuth: AngularFireAuth, private angularFireMessaging: AngularFireMessaging) {
  this.angularFireMessaging.messages.subscribe(
    (_messaging: AngularFireMessaging) => {
    _messaging.onMessage = _messaging.onMessage.bind(_messaging);
    _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
  })
}

requestPermission() {
this.angularFireMessaging.requestToken.subscribe(
(token) => {
console.log(token);
localStorage.setItem('token', JSON.stringify(token));
},
(err) => {
console.error('Unable to get permission to notify.', err);
}
);
}
receiveMessage() {
this.angularFireMessaging.messages.subscribe(
(payload) => {
console.log("new message received. ", payload);
this.currentMessage.next(payload);
})
}
}