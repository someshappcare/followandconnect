import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxSpinnerModule } from "ngx-spinner";  
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider} from 'angularx-social-login';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { NavbarComponent } from './homepage/navbar/navbar.component';
import { HomeComponent } from './homepage/home/home.component';
import { SpoolvidComponent } from './homepage/spoolvid/spoolvid.component';
import { ProfilesidenavComponent } from './profile/profilesidenav/profilesidenav.component';
import { SettingsComponent } from './profile/settings/settings.component';
import { ChangepasswordComponent } from './profile/changepassword/changepassword.component';
import { PrivacyComponent } from './profile/privacy/privacy.component';
import { HelpandsupportComponent } from './profile/helpandsupport/helpandsupport.component';
import { NotificationsComponent } from './profile/notifications/notifications.component';
import { SearchfriendsComponent } from './profile/searchfriends/searchfriends.component';
import { UserprofileComponent } from './homepage/userprofile/userprofile.component';
import { EditprofileComponent } from './homepage/editprofile/editprofile.component';
import { FollowprofileComponent } from './homepage/followprofile/followprofile.component';
import { FollowingprofileComponent } from './homepage/followingprofile/followingprofile.component';
import { FriendsprofileComponent } from './homepage/friendsprofile/friendsprofile.component';
import { IntotoComponent } from './homepage/intoto/intoto.component';
import { ChatComponent } from './homepage/chat/chat.component';
import { IntotoprivateComponent } from './homepage/intotoprivate/intotoprivate.component';
import { IntotopublicComponent } from './homepage/intotopublic/intotopublic.component';
import { AboutUsComponent } from './profile/about-us/about-us.component';
import { TermsandconditionsComponent } from './profile/termsandconditions/termsandconditions.component';
import { IntotoprivatephotosComponent } from './homepage/intotoprivatephotos/intotoprivatephotos.component';
import { IntotoprivatevideosComponent } from './homepage/intotoprivatevideos/intotoprivatevideos.component';
import { IntotopublicvideosComponent } from './homepage/intotopublicvideos/intotopublicvideos.component';
import { IntotopublicphotosComponent } from './homepage/intotopublicphotos/intotopublicphotos.component';
import { TermsComponent } from './terms/terms.component';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SpinnerComponent } from './helpers/spinner/spinner.component';
import { FullviewComponent } from './homepage/fullview/fullview.component';
import { ChatlistComponent } from './homepage/chatlist/chatlist.component';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';
import {Ng2TelInputModule} from 'ng2-tel-input';
import { ReversePipe } from './helpers/reverse.pipe';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { LinebreakPipe } from './helpers/linebreak.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    NavbarComponent,
    HomeComponent,
    SpoolvidComponent,
    ProfilesidenavComponent,
    SettingsComponent,
    ChangepasswordComponent,
    PrivacyComponent,
    HelpandsupportComponent,
    NotificationsComponent,
    SearchfriendsComponent,
    UserprofileComponent,
    EditprofileComponent,
    FollowprofileComponent,
    FollowingprofileComponent,
    FriendsprofileComponent,
    IntotoComponent,
    ChatComponent,
    IntotoprivateComponent,
    IntotopublicComponent,
    AboutUsComponent,
    TermsandconditionsComponent,
    IntotoprivatephotosComponent,
    IntotoprivatevideosComponent,
    IntotopublicvideosComponent,
    IntotopublicphotosComponent,
    TermsComponent,
    SpinnerComponent,
    FullviewComponent,
    ChatlistComponent,
    ReversePipe,
    LinebreakPipe,
    PrivacypolicyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    SocialLoginModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase),
    NgbModule,
    ToastrModule.forRoot(), 
    ShareButtonsModule,
    ShareIconsModule,
    Ng2TelInputModule,
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            // provider: new GoogleLoginProvider(
            //   '1069867256981-fqadro39am42gg4jqjgde38duvp87lhb.apps.googleusercontent.com'
            // )
            provider: new GoogleLoginProvider(
              '1069867256981-fqadro39am42gg4jqjgde38duvp87lhb.apps.googleusercontent.com'
            )
          }
        ]
      } as SocialAuthServiceConfig,
    }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
