import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { HomeComponent } from './homepage/home/home.component';
import { SpoolvidComponent } from './homepage/spoolvid/spoolvid.component';
import { SettingsComponent } from './profile/settings/settings.component';
import { NotificationsComponent } from './profile/notifications/notifications.component';
import { ChangepasswordComponent } from './profile/changepassword/changepassword.component';
import { PrivacyComponent } from './profile/privacy/privacy.component';
import { HelpandsupportComponent } from './profile/helpandsupport/helpandsupport.component';
import { SearchfriendsComponent } from './profile/searchfriends/searchfriends.component';
import { UserprofileComponent } from './homepage/userprofile/userprofile.component';
import { EditprofileComponent } from './homepage/editprofile/editprofile.component';
import { FollowprofileComponent } from './homepage/followprofile/followprofile.component';
import { FollowingprofileComponent } from './homepage/followingprofile/followingprofile.component';
import { FriendsprofileComponent } from './homepage/friendsprofile/friendsprofile.component';
import { ChatComponent } from './homepage/chat/chat.component';
import { IntotoComponent } from './homepage/intoto/intoto.component';
import { IntotoprivateComponent } from './homepage/intotoprivate/intotoprivate.component';
import { IntotopublicComponent } from './homepage/intotopublic/intotopublic.component';
import { AuthguardService } from './helpers/authguard.service';
import { AboutUsComponent } from './profile/about-us/about-us.component';
import { TermsandconditionsComponent } from './profile/termsandconditions/termsandconditions.component';
import { IntotopublicphotosComponent } from './homepage/intotopublicphotos/intotopublicphotos.component';
import { IntotoprivatephotosComponent } from './homepage/intotoprivatephotos/intotoprivatephotos.component';
import { IntotoprivatevideosComponent } from './homepage/intotoprivatevideos/intotoprivatevideos.component';
import { IntotopublicvideosComponent } from './homepage/intotopublicvideos/intotopublicvideos.component';
import { TermsComponent } from './terms/terms.component';
import { FullviewComponent } from './homepage/fullview/fullview.component';
import { ChatlistComponent } from './homepage/chatlist/chatlist.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'terms', component: TermsComponent },
  { path: 'forgotPassword', component: ForgotPasswordComponent },
  { path: 'myView', component: HomeComponent, canActivate: [AuthguardService] },
  { path: 'spoolvid', component: SpoolvidComponent },
  { path: 'intoto', component: IntotoComponent },
  { path: 'chat', component: ChatComponent },
  { path: 'chatList', component: ChatlistComponent },
  { path: 'userprofile', component: UserprofileComponent },
  { path: 'editprofile', component: EditprofileComponent },
  { path: 'followprofile', component: FollowprofileComponent },
  { path: 'followingprofile', component: FollowingprofileComponent },
  { path: 'friendsprofile', component: FriendsprofileComponent },
  { path: 'settings', component: SettingsComponent, canActivate: [AuthguardService] },
  { path: 'notification', component: NotificationsComponent },
  { path: 'changepassword', component: ChangepasswordComponent },
  { path: 'privacy', component: PrivacyComponent },
  { path: 'aboutUs', component: AboutUsComponent },
  { path: 'help', component: HelpandsupportComponent },
  { path: 'termsandconditions', component: TermsandconditionsComponent },
  { path: 'search', component: SearchfriendsComponent },
  { path: 'intotoprivate', component: IntotoprivateComponent },
  { path: 'intotopublic', component: IntotopublicComponent },
  { path: 'followProfile', component: FollowprofileComponent },
  { path: 'followingProfile', component: FollowingprofileComponent },
  { path: 'friendsProfile', component: FriendsprofileComponent },
  { path: 'intotopublicphotos', component: IntotopublicphotosComponent },
  { path: 'intotopublicvideos', component: IntotopublicvideosComponent },
  { path: 'intotoprivatephotos', component: IntotoprivatephotosComponent },
  { path: 'intotoprivatevideos', component: IntotoprivatevideosComponent },
  { path: 'fullview', component: FullviewComponent },
  { path: 'privacypolicy', component: PrivacypolicyComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes,{scrollPositionRestoration: 'enabled', useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
