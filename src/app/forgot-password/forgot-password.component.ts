import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ConstImages } from 'src/app/helpers/const-images';
import { RestapiService } from '../restapi.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  left_image = ConstImages.left_image;
  usernameCross = ConstImages.username_cross;
  logo = ConstImages.logo;
  error = '';
  forgotForm: FormGroup;
  submitted = false;
  forgotpasswordemail;
  // const images & validations
  emailValidation = ConstImages.email_validation;
  emailpatternValidation = ConstImages.emailpattern_validation;

  constructor(private toastr: ToastrService, private router: Router, private formBuilder: FormBuilder, private restservice: RestapiService, private SpinnerService: NgxSpinnerService) { }

  ngOnInit() {
    this.forgotForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  get f() { return this.forgotForm.controls; }
  onSubmit() {
    this.forgotpasswordemail = {
      "email": this.forgotForm.value.email,
    }
    this.submitted = true;
    console.log(this.forgotForm.value);
    if (this.forgotForm.invalid) {
      return;
    }
    this.SpinnerService.show();
    this.restservice.forgotPassword(this.forgotpasswordemail).subscribe((list: any) => {
      console.log(list);
      if(list.status ==false){
        this.toastr.warning('', list.message,{
          positionClass: 'toast-top-center'
     });
      this.SpinnerService.hide();
      }
      if(list.status ==true){
        this.toastr.warning('', list.message,{
          positionClass: 'toast-top-center'
     });
        this.SpinnerService.hide();
        
      this.router.navigate(['/login']);
      }
    }, err => {
      this.SpinnerService.hide();
      console.log(err);
    });
  }

  usernameclear(){
    this.forgotForm.controls['email'].reset();
  }

}