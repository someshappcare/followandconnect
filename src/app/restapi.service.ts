import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RestapiService {

  constructor(private httpClient: HttpClient) { }
  // url = "http://13.126.39.225";
  url = "https://follow-connect.com";

  login = '/socialmedia/index.php/register/login';
  sociallogin = '/socialmedia/index.php/Register/social_login';
  register = '/socialmedia/index.php/register/register';
  randomId = '/socialmedia/index.php/Register/random_username';
  help = '/socialmedia/index.php/Register/help_support';
  faq = '/socialmedia/index.php/Register/faqs';
  terms = '/socialmedia/index.php/Register/terms_and_conditions';
  about = '/socialmedia/index.php/Register/about_us';
  privacy = '/socialmedia/index.php/Register/privacy_policy';
  delete = '/socialmedia/index.php/Register/account_delete';
  changepwd = '/socialmedia/index.php/Register/change_password';
  notifctn = '/socialmedia/index.php/Networks/notificationList';
  forgotpwd = '/socialmedia/index.php/Register/forgot_password';
  block = '/socialmedia/index.php/Networks/blockerList';
  searchfrnd = '/socialmedia/index.php/Networks/UserNamesearch';
  searchlst = '/socialmedia/index.php/Networks/getSerachList';
  searchdatainsrt = '/socialmedia/index.php/Networks/SearchdataInsert';
  profile = '/socialmedia/index.php/Register/profile_details';
  edtprofile = '/socialmedia/index.php/Register/profile_update';
  frndlst = '/socialmedia/index.php/Networks/FriendsList';
  followerlst = '/socialmedia/index.php/Networks/FollowingList';
  feedpst = '/socialmedia/index.php/Feed/feedPost';
  feeddsply = '/socialmedia/index.php/Feed/getFeed';
  feedcmtlst = '/socialmedia/index.php/Feed/commentsList';
  feedcmt = '/socialmedia/index.php/Feed/userFeddactivity';
  feedlke = '/socialmedia/index.php/Feed/LikesList';
  spoolvidpst = '/socialmedia/index.php/Feed/spoolVideoUpload';
  spoolviddsply = '/socialmedia/index.php/Feed/getspoolvid';
  getuserprofilefed = '/socialmedia/index.php/Feed/getUserFeed';
  sendrqst = '/socialmedia/index.php/Networks/sendRequest';
  frndstatus = '/socialmedia/index.php/Feed/getUserFriendStaus';
  getintoto = '/socialmedia/index.php/Feed/getInTOTO';
  addfrnd = '/socialmedia/index.php/Networks/addFriend';
  blockusr = '/socialmedia/index.php/Chat/block_user';
  unfrnd = '/socialmedia/index.php/Networks/unFriend';
  unblockusr = '/socialmedia/index.php/Chat/unblock_user';
  chatfrndlst = '/socialmedia/index.php/Chat/get_friend_list';
  chathstry = '/socialmedia/index.php/Chat/ChatHistory';
  chatsnd = '/socialmedia/index.php/Chat/send';
  profilephoto = '/socialmedia/index.php/Register/profile_photo_update';
  viewfeedcount = '/socialmedia/index.php/Feed/feedView';
  deletefeed ='/socialmedia/index.php/Feed/deleteFeed';
  feededit ='/socialmedia/index.php/Feed/feedEdit';
  countryFlag ='/socialmedia/index.php/Register/CountryFlag_update';

  registerUser(data: any) {
    return this.httpClient.post(this.url + this.register, data);
  }

  loginuser(loginformdata: any) {
    return this.httpClient.post(this.url + this.login, loginformdata);
  }

  socillogin(socialloginformdata: any) {
    return this.httpClient.post(this.url + this.sociallogin, socialloginformdata);
  }

  randomUserId(id: any) {
    return this.httpClient.post(this.url + this.randomId, id);
  }
  Countriesflag(id: any) {
    return this.httpClient.post(this.url + this.countryFlag, id);
  }

  helpsupport() {
    return this.httpClient.get(this.url + this.help);
  }

  faqs() {
    return this.httpClient.get(this.url + this.faq);
  }

  termsandconditions() {
    return this.httpClient.get(this.url + this.terms);
  }

  aboutUs() {
    return this.httpClient.get(this.url + this.about);
  }

  privacypolicy() {
    return this.httpClient.get(this.url + this.privacy);
  }

  delecteAccount(account: any) {
    return this.httpClient.post(this.url + this.delete, account);
  }

  changePassword(changepassword: any) {
    return this.httpClient.post(this.url + this.changepwd, changepassword);
  }

  notifications(notification: any) {
    return this.httpClient.post(this.url + this.notifctn, notification);
  }

  forgotPassword(forgotpassword: any) {
    return this.httpClient.post(this.url + this.forgotpwd, forgotpassword);
  }

  blockUsersList(blockedlist: any) {
    return this.httpClient.post(this.url + this.block, blockedlist);
  }

  searchfriends(search: any) {
    return this.httpClient.post(this.url + this.searchfrnd, search);
  }

  searchlist(list: any) {
    return this.httpClient.post(this.url + this.searchlst, list);
  }

  searchdatainsert(search: any) {
    return this.httpClient.post(this.url + this.searchdatainsrt, search);
  }

  getuserProfile(userProfile: any) {
    return this.httpClient.post(this.url + this.profile, userProfile);
  }

  editUserProfile(editprofile: any) {
    return this.httpClient.post(this.url + this.edtprofile, editprofile);
  }

  friendsList(feed: any) {
    return this.httpClient.post(this.url + this.frndlst, feed);
  }

  followerList(feed: any) {
    return this.httpClient.post(this.url + this.followerlst, feed);
  }

  feedpost(feedpost: any) {
    console.log("service", feedpost);
    return this.httpClient.post(this.url + this.feedpst, feedpost);
  }

  feeddisplay(feed: any) {
    return this.httpClient.post(this.url + this.feeddsply, feed);
  }

  feedcommentList(commentlist: any) {
    return this.httpClient.post(this.url + this.feedcmtlst, commentlist);
  }

  feedcomment(comment: any) {
    return this.httpClient.post(this.url + this.feedcmt, comment);
  }

  feedlike(like: any) {
    return this.httpClient.post(this.url + this.feedlke, like);
  }
  edituserfeed(editfeed: any) {
    return this.httpClient.post(this.url + this.feededit, editfeed);
  }
  deleteuserfeed(deletefeed: any) {
    return this.httpClient.post(this.url + this.deletefeed, deletefeed);
  }
  spoolvidPost(feed: any) {
    return this.httpClient.post(this.url + this.spoolvidpst, feed);
  }

  spoolviddisplay(feed: any) {
    return this.httpClient.post(this.url + this.spoolviddsply, feed);
  }
  getuserprofilefeed(feed: any) {
    return this.httpClient.post(this.url + this.getuserprofilefed, feed);
  }

  sendfriendrequest(feed: any) {
    return this.httpClient.post(this.url + this.sendrqst, feed);
  }

  friendStatus(feed: any) {
    return this.httpClient.post(this.url + this.frndstatus, feed);
  }
  getIntoto(feed: any) {
    return this.httpClient.post(this.url + this.getintoto, feed);
  }

  addFriend(feed: any) {
    return this.httpClient.post(this.url + this.addfrnd, feed);
  }

  blockUser(feed: any) {
    return this.httpClient.post(this.url + this.blockusr, feed);
  }

  unfriend(feed: any) {
    return this.httpClient.post(this.url + this.unfrnd, feed);
  }

  unblockUser(unblock: any) {
    return this.httpClient.post(this.url + this.unblockusr, unblock);
  }

  chatfriendlist(feed: any) {
    return this.httpClient.post(this.url + this.chatfrndlst, feed);
  }

  chatHistory(feed: any) {
    return this.httpClient.post(this.url + this.chathstry, feed);
  }

  chatSend(feed: any) {
    return this.httpClient.post(this.url + this.chatsnd, feed);
  }
  profilephoto_update(photo: any) {
    return this.httpClient.post(this.url + this.profilephoto, photo);
  }
  viewcount(view: any) {
    return this.httpClient.post(this.url + this.viewfeedcount, view);
  }
}