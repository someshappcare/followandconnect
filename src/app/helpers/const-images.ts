export class ConstImages {

    // static images

    // common images
    public static logo = "assets/icons/logo.png";
    public static inida = "assets/india.jpeg";
    public static usa = "assets/usa.jpeg";
    public static singapore = "assets/singapore.jpeg";
    public static germany = "assets/germany.jpeg";
    public static canada = "assets/canada.jpeg";
    public static defaultpic = "assets/icons/default_pic.png";
    public static defaultpic1 = "assets/icons/default_pic1.png";

    // login& register images
    public static left_image = "assets/login_logo.png";
    public static google_icon = "assets/icons/google.png";
    public static username_cross = "assets/icons/Cross.png";
    public static upload_cross = "assets/icons/Cross@2x.png";

    // navbar
    public static profile = "assets/profile.png";
    public static userprofile = "assets/user_profile.png";
    public static posts = "assets/icons/Posts.png";
    public static logout = "assets/icons/logout-1.png";

    // userprofile
    public static userlike = "assets/icons/userlike.png";
    public static post = "assets/icons/posts.png";
    public static friends = "assets/icons/friends.png";
    public static followerprofile = "assets/followerprofile.png";
    public static following_right = "assets/icons/following-right.png";
    public static addasfriend = "assets/icons/addasfriend.png";
    public static blockUser = "assets/icons/blockUser.png";
    public static unFriend = "assets/icons/unFriend.png";

    // home page
    public static like = "assets/icons/like.png";
    public static likeColor = "assets/icons/like-color.png";
    public static dislike = "assets/icons/dislike.png";
    public static dislikeColor = "assets/icons/dislike-color.png";
    public static add = "assets/icons/add.png";
    public static eye = "assets/icons/eye-1.png";
    public static comment = "assets/icons/comment-1.png";
    public static side_dots = "assets/icons/icon-1.png";
    public static photos = "assets/photos.png";
    public static videos = "assets/videos.png";
    public static arrowright= "assets/icons/arrow-right-1.png"; 
    public static arrowleft= "assets/icons/arrow-left-1.png";

    // profile
    public static notification_image = "assets/notification-image.png";
    public static settings_rightarrow = "assets/icons/settings-rightarrow-1.png";
    public static privacy = "assets/icons/privacy-1.png";
    public static notifications = "assets/icons/notifications-1.png";
    public static settings = "assets/icons/settings-1.png";
    public static help = "assets/icons/help-1.png";
    public static search = "assets/icons/search-1.png";
    public static key = "assets/icons/key-1.png";

    // static error messages
    public static success = "200";
    public static failure = "500";
    public static error = "500";
    // static constants validations

    public static username_validation = "User Name is required";
    public static password_validation = "Password is required";
    public static email_validation = "Email is required";
    public static emailpattern_validation = "Email must be a valid";
    public static userId_validation = "UserId is required";
    public static confirmpassword_validation = "Confirm Password is required";
    public static confirmpasswordvalidator_validation = "Password and Confirm Password must match";
    public static mobileno_validation = "Mobile No is required";
    public static mobilenolength_validation = "Mobile no must be 12 Digits";
    public static mobilenodigits_validation = "Mobile no must be numbers";
    public static dob_validation = "DOB is required";
    public static gender_validation = "Gender is required";
    public static country_validation = "Country is required";
    public static oldpassword_validation = "Old Password is required";
    public static newpassword_validation = "New Password is required";

// details

    public static apikey = '827ccb0eea8a706c4c34a16891f84e7b';
}

