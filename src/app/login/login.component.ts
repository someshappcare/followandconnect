import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ConstImages } from 'src/app/helpers/const-images';
import { RestapiService } from '../restapi.service';
import { NgxSpinnerService } from "ngx-spinner";
import { SocialAuthService, GoogleLoginProvider, SocialUser } from "angularx-social-login";
import { ToastrService } from 'ngx-toastr';
import { MessagingService } from '../messaging.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  fieldTextTypePassword: boolean;

  logo = ConstImages.logo;
  userdata;
  error = '';token: any;
  loginForm: FormGroup;
  submitted = false;
  loginformdata: any; userdetails: any;
  socilloginformdata: any;
  // constants images & validtions
  left_image = ConstImages.left_image;
  googleIcon = ConstImages.google_icon;
  usernameCross = ConstImages.username_cross;
  usernameValidation = ConstImages.username_validation;
  passwordValidation = ConstImages.password_validation;
  private user: SocialUser; message: any;
  public authorized: boolean = false;

  toggleFieldPassword() {
    this.fieldTextTypePassword = !this.fieldTextTypePassword;
  }

  constructor(private messagingService: MessagingService, private toastr: ToastrService,private socialAuthService: SocialAuthService, private router: Router, private formBuilder: FormBuilder, private restservice: RestapiService, private SpinnerService: NgxSpinnerService) { }

  ngOnInit() {
    this.messagingService.requestPermission();
    this.messagingService.receiveMessage();
    this.message = this.messagingService.currentMessage;
    console.log("message", this.message);
    this.token = JSON.parse(localStorage.getItem("token"));
    if(this.token ==null){
    console.log("if token null", this.token);
    this.token = "123456";
    console.log("if token null set", this.token);
    }
    this.userdetails = JSON.parse(localStorage.getItem("currentUser"));
    if (this.userdetails!='') {
      this.router.navigate(['myView']);
    } else{
      this.router.navigate(['login']);
    }
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }
  
  public socialSignIn(socialPlatform: string) {
    console.log("socialSignIn()",this.token);
    let socialPlatformProvider;
    if (socialPlatform == "google") {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.socialAuthService.signIn(socialPlatformProvider).then(
      (data) => {
        console.log(socialPlatform + " sign in data : ", data);
        // Now sign-in with userData        
        if (data != null) {
          this.authorized = true;
          this.user = data;
          console.log("user details", data);
          this.socilloginformdata = {
            "social_uid": data.id,
            "fullname": data.name,
            'email': data.email,
            "devicetype": "web",
            "deviceid": "1234567",
            "devicetoken": this.token,
          }
          console.log(this.socilloginformdata.value);
          this.SpinnerService.show();
          this.restservice.socillogin(this.socilloginformdata).subscribe((list: any) => {
            console.log(list.status);
            if (list.status == true) {
              console.log("success");
              this.userdata = list.data;
              console.log(list.message);
              this.SpinnerService.hide();
              console.log("user details", this.userdata);
              localStorage.setItem('currentUser', JSON.stringify(this.userdata));
              this.router.navigate(['/myView']);
                localStorage.removeItem('token');
            } else {
              console.log(list.message);
              this.toastr.error('', list.message,{
                positionClass: 'toast-top-center'
           });
              this.SpinnerService.hide();
            }
          }, err => {
            this.SpinnerService.hide();
            console.log("error message" + err);
          });
        }
      });
  }

  get f() { return this.loginForm.controls; }
  onSubmit() {
    console.log("onSubmit()",this.token);
    this.loginformdata = {
      "username": this.loginForm.value.username,
      "password": this.loginForm.value.password,
      "devicetype": "web",
      "deviceid": "1234567",
      "devicetoken": this.token,
      "API-KEY": "827ccb0eea8a706c4c34a16891f84e7b"
    }
    this.submitted = true;
    console.log(this.loginForm.value);
    if (this.loginForm.invalid) {
      return;
    }
    this.SpinnerService.show();
    this.restservice.loginuser(this.loginformdata).subscribe((list: any) => {
      console.log(list.status);
      if (list.status == true) {
        console.log("success");
        this.userdata = list.data;
        this.toastr.warning('', list.message,{
          positionClass: 'toast-top-center'
     });
        this.SpinnerService.hide();
        console.log("user details", this.userdata);
        localStorage.setItem('currentUser', JSON.stringify(this.userdata));
        this.router.navigate(['/myView']);
      } else {
        console.log(list.message);
        this.toastr.warning('', list.message,{
          positionClass: 'toast-top-center'
     });
        this.SpinnerService.hide();
      }
    }, err => {
      this.SpinnerService.hide();
      console.log("error message" + err);
    });
  }

  usernameclear(){
    this.loginForm.controls['username'].reset();
  }
}